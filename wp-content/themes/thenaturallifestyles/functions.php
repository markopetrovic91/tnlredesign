<?php

add_theme_support('post-thumbnails');
add_image_size('blog_category_post_pic', 650, 400, true);
add_image_size('workshop_cat_img', 630, 620, true);
add_image_size('workshop_cat_large', 1300);

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __('Main Menu'),
    )
  );
}
add_action( 'init', 'register_my_menus' );

function get_workshop_dates($date_start_string = null, $date_end_string = null){
    if($date_start_string === null || $date_end_string === null){
        return false;
    }
    else {
        $dates = array();
        $date_start_array   = explode('-', $date_start_string);
        $date_end_array     = explode('-', $date_end_string);

        $dates['start_day']     = $date_start_array[2];
        $dates['start_month']   = get_month_short($date_start_array[1]);
        $dates['end_day']       = $date_end_array[2];
        $dates['end_month']     = get_month_short($date_end_array[1]);

        return $dates;
    }
}

function get_month_short($numeric) {
    $short = false;
    switch ($numeric) {
        case 1:
            $short = 'JAN';
            break;
        case 2:
            $short = 'FEB';
            break;
        case 3:
            $short = 'MAR';
            break;
        case 4:
            $short = 'APR';
            break;
        case 5:
            $short = 'MAY';
            break;
        case 6:
            $short = 'JUN';
            break;
        case 7:
            $short = 'JUL';
            break;
        case 8:
            $short = 'AUG';
            break;
        case 9:
            $short = 'SEP';
            break;
        case 10:
            $short = 'OCT';
            break;
        case 11:
            $short = 'NOV';
            break;
        case 12:
            $short = 'DEC';
            break;
    }
    return $short;
}

add_action('wp_ajax_nopriv_tnl_get_blog_posts', 'tnl_get_blog_posts');
add_action('wp_ajax_tnl_get_blog_posts', 'tnl_get_blog_posts');
add_action('wp_ajax_nopriv_tnl_get_blog_post', 'tnl_get_blog_post');
add_action('wp_ajax_tnl_get_blog_post', 'tnl_get_blog_post');
add_action('wp_ajax_nopriv_tnl_get_video_post', 'tnl_get_video_post');
add_action('wp_ajax_tnl_get_video_post', 'tnl_get_video_post');
add_action('wp_ajax_nopriv_tnl_post_inquiry', 'tnl_post_inquiry');
add_action('wp_ajax_tnl_post_inquiry', 'tnl_post_inquiry');
add_action('wp_ajax_nopriv_tnl_post_contact', 'tnl_post_contact');
add_action('wp_ajax_tnl_post_contact', 'tnl_post_contact');

function tnl_get_blog_posts(){
    check_ajax_referer('wp_ajax_tnl_get_blog_posts', 'security');

    $page           = $_POST['page'];
    $filter         = $_POST['filter'];
    $posts_array    = array();
    $num_of_pages   = false;

    $query = new WP_Query(array(
        'paged'             => $page,
        'post_type'         => 'post',
        'posts_per_page'    => 4,
        'category_name'     => $filter,
    ));

    if ( $query->have_posts() ) {
        $num_of_pages = $query->max_num_pages;
        while ( $query->have_posts() ) {
            $query->the_post();
            if (has_post_thumbnail()) {
                $image_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog_category_post_pic');
                $this_post['img'] = $image_src[0];
            }
            else{
                $this_post['img'] = false;
            }

            $this_post['id']            = get_the_ID();
            $this_post['title']         = wp_trim_words( get_the_title(), 10, '...' );
            $this_post['type']          = get_the_category();
            $this_post['author']        = get_the_author();
            $this_post['author_url']    = get_author_posts_url(get_the_author_meta('ID'));
            $this_post['date']          = get_the_date();
            $this_post['permalink']     = get_permalink();
            $posts_array['posts'][]     = $this_post;
        }
    }

    $posts_array['pagination']['num_of_pages']  = $num_of_pages;
    $posts_array['pagination']['current_page']  = $page;
    $posts_array['pagination']['filter']        = $filter;

    echo json_encode($posts_array);

    wp_die();
}

function tnl_get_blog_post(){
    check_ajax_referer('wp_ajax_tnl_get_blog_post', 'security');

    $post_id                = $_POST['id'];
    $post                   = get_post($post_id, ARRAY_A); 
    $post['content']        = apply_filters( 'the_content', $post['post_content'] );
    $image_src              = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');
    $post['img']            = $image_src[0];
    $post['type']           = get_the_category($post_id);
    $post['catt']           = $post['post_category'];
    $post['full_url']       = get_permalink($post_id);
    $post['author']         = get_the_author_meta('display_name', $post['post_author']);
    $post['author_url']     = get_author_posts_url(get_the_author_meta('ID'), $post['author']);
    $user_info              = get_userdata($post['post_author']);
    $post['author_slug']    = $user_info->user_login;

    echo json_encode($post);
    wp_die();
}

function tnl_get_video_post(){
    check_ajax_referer('wp_ajax_tnl_get_blog_post', 'security');

    $post_id                = $_POST['id'];
    $post                   = get_post($post_id, ARRAY_A); 
    $post['content']        = apply_filters( 'the_content', $post['post_content'] );
    $image_src              = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');
    $post['img']            = $image_src[0];
    $post['type']           = get_the_category($post_id);
    $post['catt']           = $post['post_category'];
    $post['full_url']       = get_permalink($post_id);
//    $post['video_id']   = //ayvpp_video_id get_permalink($post_id);
    $post['author']         = get_the_author_meta('display_name', $post['post_author']);
    //$post['author_url'] = get_author_posts_url(get_the_author_meta('ID'), $post['author']);
    $post['date']           = get_the_date('', $post_id);

//    $video              = new youtube_video();
    $post_video_id  = get_post_meta($post_id, '_ayvpp_video');
    $i = 1;
    foreach($post_video_id as $video_id){
        $post['video_id'] = $video_id;
        //$i++;
    }
    //$post['video_thumb']    = $video->thumb('medium');

    echo json_encode($post);
    wp_die();
}
function featured_webshop(){
    $workshops = new Pod('home_page_slider');
    if($workshops->field('featured_webshop')){
        $featured = $workshops->field('featured_webshop');
    }
    $dates              = get_workshop_dates($featured['date_start'], $featured['date_end']);
?>
    <div class="blog-single-workshop-wrapper inner-page-wrapper">
    <div class="workshop-box">
        <div class="workshop-image-wrapper">
            <?php
                //$image              = $workshops->field('image_secondary');
                //$image_url          = pods_image_url($image, null);
                //$dates              = get_workshop_dates($workshops->field('date_start'), $workshops->field('date_end'));
                $workshop_pod       = pods('workshop', $featured['pod_item_id']);
                $image              = $workshop_pod->field('image_secondary');
                $image_url          = pods_image_url($image, 'workshop_cat_large');
                $image_url_cat_vertical          = pods_image_url($image, 'workshop_cat_img');
            ?>

            <img class="workshop_cat_large" src="<?php echo $image_url; ?>" alt="<?php $featured['name']; ?>" />
            <img class="workshop_cat_img" src="<?php echo $image_url_cat_vertical; ?>" alt="<?php $featured['name']; ?>" />

        </div>
        <div class="workshop-details-wrapper">
            <h2><?php echo $featured['name'] ?></h2>
            <p class="subtitle"><?php echo $featured['subtitle'] ?></p>
            <div class="text-and-calendar">
                <div class="text"><?php echo $featured['excerpt'] ?></div>
                <div class="calendar non-mobile">
                    <div class="calendar-upper">
                        <div class="day"><?php echo $dates['start_day']; ?></div>
                        <div class="month"><?php echo $dates['start_month']; ?></div>
                    </div>
                    <div class="calendar-lower">
                        <div class="day"><?php echo $dates['end_day']; ?></div>
                        <div class="month"><?php echo $dates['end_month']; ?></div>
                    </div>
                </div>
            </div>
            <div class="coaches-and-calendar">
                <div class="coaches">
                <?php // var_dump($workshop_pod->field('coaches')); ?>
                    <?php $coaches            = $workshop_pod->field('coaches'); ?>
                    <?php $i = 0; ?>
                    <?php foreach ($coaches as $coach_a): ?>
                    <?php 
                        $coach              = pods('coach', $coach_a['id']);
                        $coach_badge        = $coach->field('badge');
                        $coach_badge_url    = pods_image_url($coach_badge, null);
                        $coach_first_name   = $coach->field('first_name');
                        $i++;
                    ?>
                    <div class="coach coach-<?php echo $coach_first_name; ?>">
                        <div class="coach-image">
                            <img src="<?php echo $coach_badge_url; ?>" />
                        </div>
                        <div class="coach-name"><?php echo $coach_first_name; ?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
<!--                 <div class="calendar mobile">
                    <div class="calendar-upper">
                        <div class="day"><?php // echo $dates['start_day']; ?></div>
                        <div class="month"><?php // echo $dates['start_month']; ?></div>
                    </div>
                    <div class="calendar-lower">
                        <div class="day"><?php // echo $dates['end_day']; ?></div>
                        <div class="month"><?php // echo $dates['end_month']; ?></div>
                    </div>
                </div> -->
            </div>
            <a class="about-info-button" href="<?php echo site_url('workshop/'.$featured['permalink'], 'relative'); ?>">More information</a>
        </div>
    </div>
</div>
<?php
}

function tnl_post_inquiry(){
    check_ajax_referer('wp_ajax_tnl_post_inquiry', 'security');

    $response = array();

    if(isset($_POST['captcha'])) {
        $captcha = $_POST['captcha'];
    }
    else{
        $response['messages']['danger'][] = 'Captcha not received';
    }
    if(!$captcha) {
        $response['valid'] = false;
        $response['messages']['danger'][] = 'Please check the the captcha form';
        $response['captcha'] = false;
    }
    else{
        $recaptcha_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lea_RYUAAAAAOEeU42g7o9eI9jpFWkxdj4B99VT&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        if($recaptcha_response.success == false) {
            $response['valid'] = false;
            $response['messages']['danger'][] = 'The system detected you as a spammer';
            $response['captcha'] = false;
        }
        else{
            $response['valid'] = true;
            $response['messages']['success'][] = 'The system did not detected you as a spammer';
            // Set up data for InfusionSoft
            $url = 'https://lb182.infusionsoft.com/app/form/process/ab31c736f88c199c2e66afa76f0f99f6';
            $fields = array(
                'inf_form_xid'                  => 'ab31c736f88c199c2e66afa76f0f99f6',
                'inf_form_name'                 => 'New Application Form TNL',
                'infusionsoft_version'          => '1.63.0.47',
                'inf_field_FirstName'           => $_POST['first_name'],
                'inf_field_LastName'            => $_POST['last_name'],
                'inf_field_Email'               => $_POST['email'],
                'inf_field_Phone1'              => $_POST['mobile'],
                'inf_custom_skype'              => $_POST['skype'],
                'inf_custom_Agegroup'           => $_POST['age'],
                'inf_custom_Ethnicgroup'        => $_POST['ethnicgroup'],
                'inf_custom_Areyouastudent0'    => $_POST['student'],
                'inf_custom_Occupation'         => $_POST['occupation'],
                'inf_custom_PreferredCoaching'  => $_POST['coaching'],
                'inf_field_Country'             => $_POST['country'],
                'inf_custom_Question'           => $_POST['question'],
            );
            // push to InfusionSoft
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');

            //open connection
            $ch = curl_init();

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            //execute post
            $result = curl_exec($ch);

            //close connection
            curl_close($ch);
            if($result != false){
                $response['c_result'] = true;
            }
            else{
                $response['c_result'] = false;   
            }
        }
    }

    echo json_encode($response);
    wp_die();
}

function tnl_post_contact(){
    check_ajax_referer('wp_ajax_tnl_post_contact', 'security');

    $response = array();

    if(isset($_POST['captcha'])) {
        $captcha = $_POST['captcha'];
    }
    else{
        $response['messages']['danger'][] = 'Captcha not received';
    }
    if(!$captcha) {
        $response['valid'] = false;
        $response['messages']['danger'][] = 'Please check the the captcha form';
        $response['captcha'] = false;
    }
    else{
        $recaptcha_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lea_RYUAAAAAOEeU42g7o9eI9jpFWkxdj4B99VT&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        if($recaptcha_response.success == false) {
            $response['valid'] = false;
            $response['messages']['danger'][] = 'The system detected you as a spammer';
            $response['captcha'] = false;
        }
        else{
            $response['valid'] = true;
            $response['messages']['success'][] = 'The system did not detected you as a spammer';
            // Set up data for InfusionSoft
            $url = 'https://lb182.infusionsoft.com/app/form/process/238ff429d0a06ba7b3240c5c7fe06bfe';
            $fields = array(
                'inf_form_xid'                  => '238ff429d0a06ba7b3240c5c7fe06bfe',
                'inf_form_name'                 => 'Contact form - New TNL',
                'infusionsoft_version'          => '1.63.0.52',
                'inf_field_FirstName'           => $_POST['first_name'],
                'inf_field_LastName'            => $_POST['last_name'],
                'inf_field_Email'               => $_POST['email'],
                'inf_field_Phone1'              => $_POST['mobile'],
                'inf_field_Country'             => $_POST['country'],
                'inf_custom_Question0'          => $_POST['question'],
            );
            // push to InfusionSoft
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');

            //open connection
            $ch = curl_init();

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            //execute post
            $result = curl_exec($ch);

            //close connection
            curl_close($ch);
            if($result != false){
                $response['c_result'] = true;
            }
            else{
                $response['c_result'] = false;   
            }
        }
    }

    echo json_encode($response);
    wp_die();
}

/*EOF*/