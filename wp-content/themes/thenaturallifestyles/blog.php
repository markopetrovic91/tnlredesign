<?php /* Template name: Blog */ ?>

<?php get_header(); ?>
<div class="social-header" style="display: none;">

</div>

<input class="blog-nonce" type="hidden" value="<?php echo wp_create_nonce("wp_ajax_tnl_get_blog_posts"); ?>" />
<input class="blog-single-nonce" type="hidden" value="<?php echo wp_create_nonce("wp_ajax_tnl_get_blog_post"); ?>" />

<div class="blog-category-wrapper inner-page-wrapper">
    <div class="blog-category-banner-wrapper">
        <div class="blog-category-banner-wrapper-inner inner-page-wrapper-inner">
            <p>Filter what's your interest from naturals</p>
            <div class="blog-filters">
                <a class="blog-filter" data-blog-filter="">All</a>
                <!-- <a class="blog-filter" data-blog-filter="vlog">vlog</a> -->
                <a class="blog-filter" data-blog-filter="blog">blog</a>
                <a class="blog-filter" data-blog-filter="video">video</a>
            </div>
        </div>
    </div>
    <div class="blog-category-wrapper-inner inner-page-wrapper-inner">
        <div class="blog-post-boxes clearfix"></div>
        <div class="blog-pagination"><ul></ul></div>
    </div>
</div>

<div class="blog-modal-wrapper-outer">
    <div class="blog-modal-background"></div>
    <div class="blog-modal-wrapper-inner">
        <div class="blog-modal-close-bttn-wrapper">
            <div class="blog-modal-close-bttn">X</div>
        </div>
        <div class="blog-modal-content-box">
            <div class="blog-modal-content-box-inner"></div>
            <div class="blog-category-social">
                <div class="blog-category-social-inner">
                    <p>Share on social networks</p>
                    <div class="fb-like" data-href="" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                    <a class="twitter-share-button" href="" data-size="large">Tweet</a>
                    <div class="g-plusone" data-annotation="none"></div>
                </div>
            </div>
        <?php featured_webshop(); ?>
<!--             <div class="post-subcontent-workshop">
                <div class="workshop-box">
                    <div class="workshop-image-wrapper">
                        <?php
                            //$image              = $workshops->field('image_secondary');
                            //$image_url          = pods_image_url($image, null);
                            //$dates              = get_workshop_dates($workshops->field('date_start'), $workshops->field('date_end'));
                        ?>
                        <img src="<?php // echo site_url('wp-content/uploads/2016/10/budapest_workshop_pages.jpg'); ?>" alt="Budapest Residential" />
                    </div>
                    <div class="workshop-details-wrapper">
                        <h2>Budapest residential</h2>
                        <p class="subtitle">Some cool subtitle or subtities</p>
                        <div class="text-and-calendar">
                            <div class="text">Lorem ipsum, tinu dinu, ide milicija bale a gde si ti? Leto je, gde si ti, ne znam gde si ti, a to bih morala znati, o-je-je-je. Noc je vrela a meni hladno je. A meni stvarno hladno je.</div>
                            <div class="calendar non-mobile">
                                <div class="calendar-upper">
                                    <div class="day">24</div>
                                    <div class="month">oct</div>
                                </div>
                                <div class="calendar-lower">
                                    <div class="day">27</div>
                                    <div class="month">oct</div>
                                </div>
                            </div>
                        </div>
                        <div class="coaches-and-calendar">
                            <div class="coaches">
                                <?php //$coaches            = $workshops->field('coaches'); ?>
                                <?php //$i = 0; ?>
                                <?php //foreach ($coaches as $coach_a): ?>
                                <?php 
                                    //$coach              = pods('coach', $coach_a['id']);
                                    //$coach_badge        = $coach->field('badge');
                                    //$coach_badge_url    = pods_image_url($coach_badge, null);
                                    //$coach_first_name   = $coach->field('first_name');
                                    //$i++;
                                ?>
                                <div class="coach coach-james">
                                    <div class="coach-image">
                                        <img src="<?php // echo site_url('wp-content/uploads/2016/10/coach_badge_james_b_140.png'); ?>" />
                                    </div>
                                    <div class="coach-name">James</div>
                                </div>
                                <div class="coach coach-james">
                                    <div class="coach-image">
                                        <img src="<?php // echo site_url('wp-content/uploads/2016/10/coach_badge_james_b_140.png'); ?>" />
                                    </div>
                                    <div class="coach-name">James</div>
                                </div>
                                <?php //endforeach; ?>
                            </div>
                            <div class="calendar mobile">
                                <div class="calendar-upper">
                                    <div class="day">24</div>
                                    <div class="month">oct</div>
                                </div>
                                <div class="calendar-lower">
                                    <div class="day">27</div>
                                    <div class="month">oct</div>
                                </div>
                            </div>
                        </div>
                        <a class="about-info-button" href="#">More information</a>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
</div>

<div class="video-modal-wrapper-outer">
    <div class="video-modal-background"></div>
    <div class="video-modal-wrapper-inner">
        <div class="video-modal-content-box">
            <div class="video-modal-content-box-inner">
                <div class="video-modal-close-bttn">X</div>
                <div class="video-modal-content-box-inner-content"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">var this_ajax_url = '<?php echo admin_url('admin-ajax.php', 'relative'); ?>';</script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/blog_category.js"></script>

<div class="social-footer" style="display: none;">

</div>

<?php get_footer();