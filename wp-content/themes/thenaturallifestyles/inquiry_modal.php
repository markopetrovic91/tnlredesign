<div class="inquiry-modal-wrapper-outer">
    <div class="inquiry-modal-background"></div>
    <div class="inquiry-modal-wrapper-inner">
        <div class="inquiry-modal-close-bttn-wrapper">
            <div class="inquiry-modal-navigation">
                <div class="inquiry-modal-navigation-item inquiry-modal-navigation-item-1 active">
                    <div class="inquiry-modal-navigation-item-inner">
                        <div class="inquiry-modal-navigation-item-number">1</div>
                        <div class="inquiry-modal-navigation-item-text">Basic Info<span class="right-arrow">></span></div>
                    </div>
                </div>
                <div class="inquiry-modal-navigation-subdesktop-arrow">></div>
                <div class="inquiry-modal-navigation-item inquiry-modal-navigation-item-2">
                    <div class="inquiry-modal-navigation-item-inner">
                        <div class="inquiry-modal-navigation-item-number">2</div>
                        <div class="inquiry-modal-navigation-item-text">Interest<span class="right-arrow">></span></div>
                    </div>
                </div>
                <div class="inquiry-modal-navigation-subdesktop-arrow">></div>
                <div class="inquiry-modal-navigation-item inquiry-modal-navigation-item-3">
                    <div class="inquiry-modal-navigation-item-inner">
                        <div class="inquiry-modal-navigation-item-number">3</div>
                        <div class="inquiry-modal-navigation-item-text">Confirm</span></div>
                    </div>
                </div>
            </div>
            <div class="inquiry-modal-close-bttn"><div>Close</div><div>X</div></div>
        </div>
        <div class="inquiry-modal-form-wrapper">
            <div class="inquiry-modal-form-wrapper-inner">
                <div class="inquiry-modal-steps">
                    <div class="inquiry-modal-step inquiry-modal-step-1">
                        <div class="inquiry-modal-step-1-left">
                            <h2>1. Basic Info</h2>
                            <p>We need to receive sincere information from you, so that we could help you.<br />Try to be open as much as you can</p>
                            <div class="inquiry-modal-step-1-form-wrapper">
                                <form id="inquiry-form-1">
                                    <div class="inquiry-modal-form-left">
                                        <div class="inquiry-modal-input-wrapper">
                                            <input type="text" class="first-name" name="first_name" placeholder="First Name" />
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <input type="text" class="last-name" name="last_name" placeholder="Last Name" />
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <input type="text" class="email" name="email" placeholder="E-mail" />
                                            <div class="required-star">*</div>
                                        </div>
                                    </div>
                                    <div class="inquiry-modal-form-right">
                                        <div class="inquiry-modal-input-wrapper">
                                            <input type="text" class="phone phone-type-input" name="phone" placeholder="Phone" />
                                            <div class="required-star">*</div>
                                            <script>
                                                jQuery(".phone-type-input").intlTelInput();
                                            </script>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <input type="text" class="skype" name="skype" placeholder="Skype" />
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <button class="inquiry-next-button inquiry-next-button-step-1">Next</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="inquiry-modal-step-1-right">
                            <img src="<?php bloginfo('template_directory'); ?>/img/inquiry_modal_step_1_pic.jpg" />
                        </div>
                    </div>
                    <div class="inquiry-modal-step inquiry-modal-step-2">
                        <div class="inquiry-modal-step-2-inner">
                            <h2>2. Interest</h2>
                            <p></p>
                            <div class="inquiry-modal-step-2-form-wrapper">
                                <form id="inquiry-form-2">
                                    <div class="inquiry-modal-form-left">
                                        <div class="inquiry-modal-input-wrapper">
                                            <select class="age" name="age"><option value="">Select your age group</option><option value="Under 18">Under 18</option><option value="18 - 21">18 - 21</option><option value="22 - 25">22 - 25</option><option value="26 - 30">26 - 30</option><option value="31 - 35">31 - 35</option><option value="36 - 40">36 - 40</option><option value="41 - 45">41 - 45</option><option value="46 - 50">46 - 50</option><option value="51 - 55">51 - 55</option><option value="56 - 60">56 - 60</option><option value="60 or more">60 or more</option></select>
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <select class="ethnicgroup" name="ethnicgroup"><option value="">What describes you best?</option><option value="White">White</option><option value="Hispanic">Hispanic</option><option value="Latino or Spanish origin">Latino or Spanish origin</option><option value="Black or African American">Black or African American</option><option value="South Asian/Indian">South Asian/Indian</option><option value="East Asian">East Asian</option><option value="Middle Eastern or North African">Middle Eastern or North African</option><option value="Mixed">Mixed</option><option value="Other">Other</option></select>
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <select class="student" name="student"><option value="">Are you a student?</option><option value="I am">I am</option><option value="I am not">I am not</option></select>
                                            <div class="required-star">*</div>
                                        </div>
                                    </div>
                                    <div class="inquiry-modal-form-center">
                                        <div class="inquiry-modal-input-wrapper">
                                            <input type="text" class="occupation" name="occupation" placeholder="Occupation" />
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <select class="coaching" name="coaching"><option value="">Preffered Coaching</option><option value="4 Day Residential">4 Day Residential</option><option value="7 Day Residential">7 Day Residential</option><option value="EuroTour">EuroTour</option><option value="Skype Coaching">Skype Coaching</option></select>
                                            <div class="required-star">*</div>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <select class="country" name="country"><option value="">Please select one</option><option value="Afghanistan">Afghanistan</option><option value="Åland Islands">Åland Islands</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="American Samoa">American Samoa</option><option value="Andorra">Andorra</option><option value="Angola">Angola</option><option value="Anguilla">Anguilla</option><option value="Antarctica">Antarctica</option><option value="Antigua and Barbuda">Antigua and Barbuda</option><option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Aruba">Aruba</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahamas (the)">Bahamas (the)</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia (Plurinational State of)">Bolivia (Plurinational State of)</option><option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option><option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="Botswana">Botswana</option><option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option><option value="British Indian Ocean Territory (the)">British Indian Ocean Territory (the)</option><option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cabo Verde">Cabo Verde</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Cayman Islands (the)">Cayman Islands (the)</option><option value="Central African Republic (the)">Central African Republic (the)</option><option value="Chad">Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island">Christmas Island</option><option value="Cocos (Keeling) Islands (the)">Cocos (Keeling) Islands (the)</option><option value="Colombia">Colombia</option><option value="Comoros (the)">Comoros (the)</option><option value="Congo (the Democratic Republic of the)">Congo (the Democratic Republic of the)</option><option value="Congo (the)">Congo (the)</option><option value="Cook Islands (the)">Cook Islands (the)</option><option value="Costa Rica">Costa Rica</option><option value="Côte d'Ivoire">Côte d'Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Curaçao">Curaçao</option><option value="Cyprus">Cyprus</option><option value="Czech Republic (the)">Czech Republic (the)</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican Republic (the)">Dominican Republic (the)</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (the) [Malvinas]">Falkland Islands (the) [Malvinas]</option><option value="Faroe Islands (the)">Faroe Islands (the)</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French Guiana">French Guiana</option><option value="French Polynesia">French Polynesia</option><option value="French Southern Territories (the)">French Southern Territories (the)</option><option value="Gabon">Gabon</option><option value="Gambia (the)">Gambia (the)</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guernsey">Guernsey</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option><option value="Holy See (the)">Holy See (the)</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option><option value="Isle of Man">Isle of Man</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jersey">Jersey</option><option value="Johnston Island">Johnston Island</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="Korea (the Democratic People's Republic of)">Korea (the Democratic People's Republic of)</option><option value="Korea (the Republic of)">Korea (the Republic of)</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People's Democratic Republic (the)">Lao People's Democratic Republic (the)</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libya">Libya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macao">Macao</option><option value="Macedonia (the former Yugoslav Republic of)">Macedonia (the former Yugoslav Republic of)</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Marshall Islands (the)">Marshall Islands (the)</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="Micronesia (Federated States of)">Micronesia (Federated States of)</option><option value="Midway Islands">Midway Islands</option><option value="Moldova (the Republic of)">Moldova (the Republic of)</option><option value="Monaco">Monaco</option><option value="Mongolia">Mongolia</option><option value="Montenegro">Montenegro</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands (the)">Netherlands (the)</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger (the)">Niger (the)</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Northern Mariana Islands (the)">Northern Mariana Islands (the)</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palau">Palau</option><option value="Palestine, State of">Palestine, State of</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines (the)">Philippines (the)</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Qatar">Qatar</option><option value="Réunion">Réunion</option><option value="Romania">Romania</option><option value="Russian Federation (the)">Russian Federation (the)</option><option value="Rwanda">Rwanda</option><option value="Saint Barthélemy">Saint Barthélemy</option><option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Martin (French part)">Saint Martin (French part)</option><option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="Samoa">Samoa</option><option value="San Marino">San Marino</option><option value="Sao Tome and Principe">Sao Tome and Principe</option><option value="Saudi Arabia">Saudi Arabia</option><option value="Senegal">Senegal</option><option value="Serbia">Serbia</option><option value="Seychelles">Seychelles</option><option value="Sierra Leone">Sierra Leone</option><option value="Singapore">Singapore</option><option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="South Sudan">South Sudan</option><option value="Southern Rhodesia">Southern Rhodesia</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan (the)">Sudan (the)</option><option value="Suriname">Suriname</option><option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic">Syrian Arab Republic</option><option value="Taiwan (Province of China)">Taiwan (Province of China)</option><option value="Tajikistan">Tajikistan</option><option value="Tanzania, United Republic of">Tanzania, United Republic of</option><option value="Thailand">Thailand</option><option value="Timor-Leste">Timor-Leste</option><option value="Togo">Togo</option><option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks and Caicos Islands (the)">Turks and Caicos Islands (the)</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates (the)">United Arab Emirates (the)</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Minor Outlying Islands (the)">United States Minor Outlying Islands (the)</option><option value="Upper Volta">Upper Volta</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela (Bolivarian Republic of)">Venezuela (Bolivarian Republic of)</option><option value="Viet Nam">Viet Nam</option><option value="Virgin Islands (British)">Virgin Islands (British)</option><option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option><option value="Wallis and Futuna">Wallis and Futuna</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select>
                                            <div class="required-star">*</div>
                                        </div>
                                    </div>
                                    <div class="inquiry-modal-form-right">
                                        <div class="inquiry-modal-input-wrapper inquiry-modal-textarea-wrapper">
                                            <textarea class="questions" name="questions" placeholder="Any Questions?"></textarea>
                                        </div>
                                        <div class="inquiry-modal-input-wrapper">
                                            <button class="inquiry-next-button inquiry-next-button-step-2">Next</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="inquiry-modal-step-2-bottom">
                                <p><span>The Natural Lifestyles</span> does not discriminate on the basis of race, color, religion (creed), age, national origin, disability, marital status, sexual orientation or military status, in any of it’s activities or operations. The above information is used in assisting us to providing the best coaching services possible.</p>
                            </div>
                        </div>
                    </div>
                    <div class="inquiry-modal-step inquiry-modal-step-3r">
                        <div class="inquiry-modal-step-3-inner">
                            <div class="inquiry-modal-step-3r-left">
                                <div class="inquiry-modal-step-3r-left-inner">
                                    <h2>Confirm</h2>
                                    <p class="subheader">We are naturals, not robots! Identify yourself!</p>
                                    <div class="recaptcha">
                                        <div id="inquiry-captcha" class="recaptcha-inner"></div>
                                    </div>
                                    <div class="result-msg"></div>
                                    <button>Submit</button>
                                    <input class="inquiry-nonce" type="hidden" value="<?php echo wp_create_nonce("wp_ajax_tnl_post_inquiry"); ?>" />
                                    <p class="notice"><span>Notice:</span> We will use this information to process your requests. But we will not intentionally sell, share or distribute your personal information to third parties.</p>
                                </div>
                            </div>
                            <div class="inquiry-modal-step-3r-right"></div>
                        </div>
                    </div>
                    <div class="inquiry-modal-step inquiry-modal-step-3p">
                        <div class="inquiry-modal-step-3-inner">
                            <div class="inquiry-modal-step-3p-left">
                                <div class="inquiry-modal-step-3p-left-inner">
                                    <h2>3. Application</h2>
                                    <p>Thanks for your inquiry about coaching with The Natural Lifestyles team.</p>
                                    <p>It’s important for you to know that our workshops are highly personalized and tailored for working professionals with the financial means to attend. We run the most intense, high end seduction coaching available worldwide and our prices reflect this.</p>
                                    <p>Alongside the cost of the workshop, please note that you’ll also be responsible for flights to the workshop location, living expenses and an allowance for style coaching.</p>
                                    <p>The Natural Lifestyles welcomes clients from all backgrounds and does not discriminate due to race, religion, colour or ethnicity. Whilst we endeavour to provide coaching solutions to everyone we have however found that students and individuals from certain countries do not usually have the means to attend due to economic circumstances in their country that are regrettably beyond their control.</p>
                                    <p>Please don't take any offence but this note is being sent to you as part of our efforts to effectively sort through the hundred's of inquiries we receive weekly.</p>
                                    <p>However, if you consider yourself to have unique financial circumstances and you really would like to get this area of your life handled in person with The Natural Lifestyles, please email me at <a href="mailto:shaun@thenaturallifestyles.com">shaun@thenaturallifestyles.com</a>, and state:</p>
                                    <ul>
                                        <li>Information on your financial situation and budget (How you have managed to set aside considerable funds for personal development)</li>
                                        <li>Your top 3 reasons why you would like to come for a workshop</li>
                                        <li>Your current situation with dating/relationships with women</li>
                                    </ul>
                                    <p>Do note that we also do not offer internships in exchange for coaching or discounts of any kind.</p>
                                    <h3>5 Principles of Natural Seduction </h3>
                                    <p>The fastest and most economical way for you to receive training from The Natural Lifestyles is through James Marshall's full Natural Seduction system - The 5 Principles.</p>
                                    <p><img src="<?php bloginfo('template_directory'); ?>/img/5principles_product_image_001.png" alt="Five principles of natural seduction"></p>
                                    <p>This 6 week home study online course takes you through a series of in depth classes on each principle and practical application for you to practice when approaching.</p>
                                    <p>You also receive weekly drills, meditation audios, plus many hours of bonus interviews and infield seduction breakdowns.</p>
                                    <p>For full details on the 5 Principles course click <a href="<?php echo site_url('products');?>">HERE</a> and you'll be sent full details.</p>
                                    <a class="button" href="<?php echo site_url('products');?>">Go to 5 Principles</a>
                                </div>
                            </div>
                            <div class="inquiry-modal-step-3p-right">
                                <div class="inquiry-modal-step-3p-right-inner">
                                    <div class="shaun-box">
                                        <div class="skype-coach-badge"></div>
                                        <div class="skype-coach-name">Shaun Masters</div>
                                        <div class="skype-coach-description">(Client Manager)</div>
                                    </div>
                                    <div class="shaun-text">Our Client Manager Shaun completed his EuroTour in 2014 and joined the company soon after. He’s excited to discuss your personal situation, answer any questions and assist you in choosing the best coaching option for you.</div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="inquiry-modal-step inquiry-modal-step-3s">
                        <div class="inquiry-modal-step-3-inner">
                            <div class="inquiry-modal-step-3s-left">
                                <h2>3. Skype sessions</h2>
                                <p>Please select your preffered coach in a drop down menu bellow</p>
                                <div class="inquiry-modal-input-wrapper">
                                    <select class="skype-coach" name="skype_coach">
                                        <option value="liam">Preffered Coach?</option>
                                        <option value="liam">Liam McRae</option>
                                        <option value="tony">Tony Solo</option>
                                        <option value="shae">Shae Matthews</option>
                                        <option value="andy">Andy Travel Bum</option>
                                    </select>
                                    <div class="required-star">*</div>
                                </div>
                            </div>
                            <div class="inquiry-modal-step-3s-center">
                                <div class="skype-coach-box skype-coach-box-liam active" data-coach="liam">
                                    <div class="skype-coach-badge"></div>
                                    <div class="skype-coach-name">Liam McRae</div>
                                    <div class="skype-coach-description">(Social freedom spec.)</div>
                                </div>
                                <div class="skype-coach-box skype-coach-box-tony" data-coach="tony">
                                    <div class="skype-coach-badge"></div>
                                    <div class="skype-coach-name">Tony Solo</div>
                                    <div class="skype-coach-description">(Tony's spec.)</div>
                                </div>
                                <div class="skype-coach-box skype-coach-box-shae" data-coach="shae">
                                    <div class="skype-coach-badge"></div>
                                    <div class="skype-coach-name">Shae Matthews</div>
                                    <div class="skype-coach-description">(Shae's spec.)</div>
                                </div>
                                <div class="skype-coach-box skype-coach-box-andy" data-coach="andy">
                                    <div class="skype-coach-badge"></div>
                                    <div class="skype-coach-name">Andy Travel Bum</div>
                                    <div class="skype-coach-description">(Andy 's spec.)</div>
                                </div>
                            </div>
                            <div class="inquiry-modal-step-3s-right">
                                <div class="skype-coach-text skype-coach-text-liam active" data-coach="liam">
                                    <p>Liam is a specialist in social freedom and rapid escalation. During the course of your sessions with him he will identify the blocks you are experiencing and prescribe personalized missions to help you take the next step in your seduction journey.</p>
                                    <p>Sessions are typically 60 minutes long, or can be divided into 3 20 minutes blocks.</p>
                                    <p class="price">$250 USD</p>
                                    <a class="button" target="_blank" href="https://thenaturallifestyles.clickfunnels.com/skype-session-with-liam-mcrae">Click here to buy</a>
                                </div>
                                <div class="skype-coach-text skype-coach-text-tony" data-coach="tony">
                                    <p>Tony Solo is a specialist in social freedom and rapid escalation. During the course of your sessions with him he will identify the blocks you are experiencing and prescribe personalized missions to help you take the next step in your seduction journey.</p>
                                    <p>Sessions are typically 60 minutes long, or can be divided into 3 20 minutes blocks.</p>
                                    <p class="price">$250 USD</p>
                                    <a class="button" target="_blank" href="https://thenaturallifestyles.clickfunnels.com/skype-session-with-tony-solo">Click here to buy</a>
                                </div>
                                <div class="skype-coach-text skype-coach-text-shae" data-coach="shae">
                                    <p>Shae Matthews is a specialist in social freedom and rapid escalation. During the course of your sessions with him he will identify the blocks you are experiencing and prescribe personalized missions to help you take the next step in your seduction journey.</p>
                                    <p>Sessions are typically 60 minutes long, or can be divided into 3 20 minutes blocks.</p>
                                    <p class="price">$250 USD</p>
                                    <a class="button" target="_blank" href="https://thenaturallifestyles.clickfunnels.com/skype-coaching-package-with-shae-matthews">Click here to buy</a>
                                </div>
                                <div class="skype-coach-text skype-coach-text-andy" data-coach="andy">
                                    <p>Andy Travel Bum is a specialist in social freedom and rapid escalation. During the course of your sessions with him he will identify the blocks you are experiencing and prescribe personalized missions to help you take the next step in your seduction journey.</p>
                                    <p>Sessions are typically 60 minutes long, or can be divided into 3 20 minutes blocks.</p>
                                    <p class="price">$250 USD</p>
                                    <a class="button" target="_blank" href="https://thenaturallifestyles.clickfunnels.com/skype-session-with-andy-travel-bum">Click here to buy</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">var this_ajax_url = '<?php echo admin_url('admin-ajax.php', 'relative'); ?>';</script>