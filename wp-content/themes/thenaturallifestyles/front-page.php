<?php get_header('front'); ?>
<div id="fullpage">
    <div class="section first-section">
        <div class="subsection intro-video-subsection">
<!--             <iframe src="https://player.vimeo.com/video/167098319?autoplay=0&title=0&byline=0&portrait=0" width="1600" height="1200" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
        <video id="james-video">
            <source src="/wp-content/themes/thenaturallifestyles/video/james.mp4" type="video/mp4">
            <source src="/wp-content/themes/thenaturallifestyles/video/james.webm" type="video/webm">
            <source src="/wp-content/themes/thenaturallifestyles/video/james.ogv" type="video/ogv">
        </video>
        <script type="text/javascript">
            $('.intro-video-subsection').on('click', function() {
                if ($(this).hasClass('playing')) {
                    $('#james-video').get(0).pause();
                    $(this).find('#play-button .pause-spec').addClass('hidden');
                    $(this).find('#play-button .play-spec').removeClass('hidden');
                    $(this).removeClass('playing');
                }else{
                    $('#james-video').get(0).play();
                    $(this).find('#play-button .pause-spec').removeClass('hidden');
                    $(this).find('#play-button .play-spec').addClass('hidden');
                    $(this).addClass('playing');
                }
            $('.intro-video-subsection #james-video').on('ended', function(){
                $(this).get(0).currentTime = 0;
                $('#play-button .pause-spec').addClass('hidden');
                $('#play-button .play-spec').removeClass('hidden');

            });
                
                
            });
        </script>
<!--         <iframe width="1600" height="1200" src="https://www.youtube.com/embed/W0j4JBs16Nc" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe> -->
    <svg id="play-button" viewBox="0 0 60 60">
      <title>play icon</title>
      <g>
        <path class="play-spec" fill="#f1f1f1" d="M24.89,40.84c-0.37,0.22-0.83,0.23-1.2,0.02s-0.6-0.61-0.6-1.04V20.2c0-0.43,0.23-0.83,0.6-1.04
        c0.37-0.21,0.83-0.21,1.2,0.02l16.35,9.81c0.36,0.21,0.58,0.6,0.58,1.02s-0.22,0.81-0.58,1.02L24.89,40.84z" />
        <path class="pause-spec hidden" fill="#f1f1f1" d="M28.03,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12c-0.86,0-1.56-0.7-1.56-1.56V19.06
        c0-0.86,0.7-1.56,1.56-1.56h3.12C27.33,17.5,28.03,18.2,28.03,19.06z M38.46,19.06v21.88c0,0.86-0.7,1.56-1.56,1.56h-3.12
        c-0.86,0-1.56-0.7-1.56-1.56V19.06c0-0.86,0.7-1.56,1.56-1.56h3.12C37.76,17.5,38.46,18.2,38.46,19.06z" />
        <path class="stroke-bg" fill="none" stroke="#999" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
    c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
        <path class="stroke" fill="none" stroke="#f1f1f1" stroke-width="4" d="M30,7C17.32,7,7,17.32,7,30
    c0,12.68,10.32,23,23,23c12.68,0,23-10.32,23-23C53,17.32,42.68,7,30,7z" />
      </g>
    </svg>
        </div>
        <div class="subsection first-section-bg-image"></div>
        <div class="pseudo-form-wrapper">
            <h1>How to finally take control of your dating life, and become natural with women</h1>
            <p>Click on the button bellow to discover the first steps to transforming your dating life, once and for all.</p>
<!--             <div class="pseudo-form home-page-opt-in">
                <div class="pseudo-form-inner">
                    <div class="email-box">
                        <input type="text" name="email" placeholder="Enter your e-mail here" />
                        <a class="pseudo-submit"></a>
                    </div>
                </div>
                <div class="pseudo-form-inner">
                    <div class="terms-box">
                        <input type="checkbox" name="agree" />
                        <div>I agree with <a href="#">Terms and Conditions</a></div>
                    </div>
                </div>
            </div> -->
            <div class="opt-in-info-wrapper">
                <a class="opt-in-info-button">More information</a>
                <script type="text/javascript">
                $('.opt-in-info-button').click(function(){
                    $('.first-section-opt-in-modal').fadeIn('slow');
                });

                </script>
            </div>
        </div>
        <div class="first-section-opt-in-modal">
            <a class="close-bttn"></a>
            <script type="text/javascript">
                $('.first-section-opt-in-modal .close-bttn').click(function(){
                    $('.first-section-opt-in-modal').fadeOut('slow');
                });
            </script>
            <div class="first-section-opt-in-modal-inner">
                <h3>Hello</h3>
                <div class="first-section-opt-in-modal-text">
                    <div class="first-section-opt-in-modal-text-inner">
                        <p>Spicy jalapeno bacon ipsum dolor amet eu chuck lorem, venison commodo strip steak shankle capicola officia do cillum. Ground round quis ut, beef ullamco capicola short loin laborum sed doner pork loin cillum. Prosciutto cupidatat velit capicola aliqua. Incididunt strip steak leberkas meatloaf venison magna biltong sunt ham hock t-bone ham dolore flank pork chop irure.</p>
                    </div>
                </div>
                <div class="pseudo-form home-page-opt-in">
                <form accept-charset="UTF-8" action="https://lb182.infusionsoft.com/app/form/process/f8912304bad2c5cea5eba9d0f5e3f583" class="infusion-form" id="infusion-form-optin" method="POST">
                    <input name="inf_form_xid" type="hidden" value="f8912304bad2c5cea5eba9d0f5e3f583" />
                    <input name="inf_form_name" type="hidden" value="TNL Optin" />
                    <input name="infusionsoft_version" type="hidden" value="1.63.0.52" />
                    <div class="infusion-field">
                        <div class="input-box first-name">
                            <input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="First Name" />
                        </div>
                        <!-- <label for="inf_field_FirstName">First Name *</label> -->
                        <!-- <input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" type="text" /> -->
                    </div>
                    <div class="infusion-field">
                        <!-- <label for="inf_field_Email">Email *</label> -->
                        <div class="input-box email">
                        <!-- <input type="text" placeholder="E-mail" /> -->
                        <input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="E-mail" />
                        </div>
                    </div>
                    <div class="infusion-submit">
                        <div class="input-box submit">
                            <button type="submit">Submit my email address</button>
                            <!-- <input type="submit" value="Submit" /> -->
                        </div>

                    </div>
                </form>
                <script type="text/javascript" src="https://lb182.infusionsoft.com/app/webTracking/getTrackingCode"></script>

                    
                    
                </div>
            </div>
        </div>
    </div>
    <div class="section second-section fp-auto-height-responsive">
        <?php $slider_workshops = new Pod('home_page_slider'); ?>
        <?php if($slider_workshops->field('workshops_in_slider')) :?>
        <?php $slider_workshops_array = $slider_workshops->field('workshops_in_slider'); ?>
        <?php else: ?>
        <?php $slider_workshops_array = false; ?>
        <?php endif; ?>
        <?php $i = 0; ?>
        <div class="second-section-slider">
            <?php foreach($slider_workshops_array as $slider_workshop) : ?>
            <?php $i++; ?>
            <?php
                $workshop_pod       = pods('workshop', $slider_workshop['pod_item_id']);
                $image              = $workshop_pod->field('image_primary');
                $image_url          = pods_image_url($image, null);
                $dates              = get_workshop_dates($slider_workshop['date_start'], $slider_workshop['date_end']);
            ?>
            <?php $num_ws = count($slider_workshops_array); ?>
            <div class="second-section-slider-slide slide slide-<?php echo $i; ?>" style="background-image: url('<?php echo $image_url; ?>');" data-slide="<?php echo $i - 1; ?>">
                <div class="slide-inner">
                    <div class="workshop-name-wrapper">
                        <h2><?php echo $slider_workshop['title_big_letters']; ?><span><?php echo $slider_workshop['title_small_letters']; ?></span></h2>
                    </div>
                    <div class="workshop-date-wrapper">
                        <div class="workshop-date-upper"><?php echo $dates['start_month']; ?></div>
                        <div class="workshop-date-middle">
                            <div class="workshop-date-middle-left"><?php echo $dates['start_day']; ?></div>
                            <div class="workshop-date-middle-right"><?php echo $dates['end_day']; ?></div>
                        </div>
                        <div class="workshop-date-bottom"><?php echo $dates['end_month']; ?></div>
                    </div>
                    <div class="workshop-info-wrapper">
                        <a href="<?php echo site_url('workshop/'.$slider_workshop['permalink']); ?>" class="workshop-info-button">More information</a>
                    </div>
                    <div class="workshop-pseudo-navigation <?php if ($num_ws > 4) {echo "show";} ?>">
                        <div class="workshop-pseudo-navigation-item"><?php echo $slider_workshop['name'] ?></div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="workshop-navigation-wrapper <?php if ($num_ws > 4) {echo "morethan";} ?>">
                
            <?php if ($num_ws < 5) { ?>
                <?php $i = 0; ?>
                <?php foreach($slider_workshops_array as $slider_workshop) : ?>
                    <a class="workshop-<?php echo $i; ?> workshop-navigation-button" data-slide="<?php echo $i; ?>"><?php echo $slider_workshop['name']; ?></a>
            <?php $i++; ?>
            <?php endforeach; ?>
            <?php   }else{
                    ?>
                    <div class="subdesktop-workshop-navigation-wrapper">
                        <div class="subdesktop-workshop-navigation-inner">
                            <a class="workshop-prev"></a>
                            <a class="workshop-next"></a>

                        </div>
                    </div>
                    <?php
                 }
                ?>


            </div>
            <script type="text/javascript">
                $('.workshop-navigation-wrapper .workshop-navigation-button').click(function(){
                    var slide_number = $(this).attr('data-slide');
                    $.fn.fullpage.moveTo('second', slide_number);
                });
            </script>
            <div class="subdesktop-workshop-navigation-wrapper">
                <div class="subdesktop-workshop-navigation-inner">
                    <a class="workshop-prev"></a>
                    <a class="workshop-next"></a>
                    <script type="text/javascript">
                        $('.workshop-prev').click(function(){
                            var active_slide    = $('.second-section .slide.active').attr('data-slide');
                            var num_of_slides   = $('.second-section .slide').length - 1;
                            var move_to         = active_slide - 1;
                            if(move_to < 0) {
                                move_to = num_of_slides;
                            }
                            $.fn.fullpage.moveTo('second', move_to);
                        });
                        $('.workshop-next').click(function(){
                            var active_slide    = parseInt($('.second-section .slide.active').attr('data-slide'));
                            var num_of_slides   = $('.second-section .slide').length;
                            var move_to         = (active_slide + 1) % num_of_slides;
                            $.fn.fullpage.moveTo('second', move_to);
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="section third-section fp-auto-height-responsive">
        <div class="third-section-left subsection"></div>
        <div class="third-section-right subsection"></div>
        <div class="third-section-main-content">
            <h2>Who we are?</h2>
            <p>We at The Natural Lifestyles specialize in taking men from a dating life of scarcity and little abundance of women, to giving them the freedom and choice that they desire in their relationships. Instead of forcing you to put on an act to try and attract women, we give you the steps to take what’s naturally attractive about you and use it to your advantage.</p>
            <a href="/about-us" class="about-info-button">More information</a>
            <script type="text/javascript">
                // $('.third-section .about-info-button').click(function(){
                //     $('.third-section-modal').fadeIn('slow');
                // });
            </script>
        </div>
        <div class="third-section-social-buttons">
            <?php $tnl_social_links = new Pod('tnl_social_links'); ?>
            <a href="<?php echo $tnl_social_links->field('facebook_link'); ?>" class="facebook" target="_blank"></a>
            <a href="<?php echo $tnl_social_links->field('instagram_link'); ?>" class="instagram" target="_blank"></a>
            <a href="<?php echo $tnl_social_links->field('youtube_link'); ?>" class="youtube" target="_blank"></a>
        </div>
        <div class="third-section-modal">
            <a class="close-bttn"></a>
            <script type="text/javascript">
                $('.third-section-modal .close-bttn').click(function(){
                    $('.third-section-modal').fadeOut('slow');
                });
            </script>
            <div class="third-section-modal-inner">
                <h3>The Natural Lifestyles</h3>
                <div class="third-section-modal-text">
                    <div class="third-section-modal-text-inner">
                        <p>Spicy jalapeno bacon ipsum dolor amet eu chuck lorem, venison commodo strip steak shankle capicola officia do cillum. Ground round quis ut, beef ullamco capicola short loin laborum sed doner pork loin cillum. Prosciutto cupidatat velit capicola aliqua. Incididunt strip steak leberkas meatloaf venison magna biltong sunt ham hock t-bone ham dolore flank pork chop irure.</p>
                        <p>Cillum ad kevin ribeye. Est mollit jerky, tri-tip rump sed flank qui ut pork belly tongue pastrami consequat short ribs aliqua. Irure jerky kielbasa, pork belly laboris lorem picanha eiusmod tenderloin shoulder chicken incididunt do pig landjaeger. Anim ut elit nulla quis sint capicola, tail meatball in. Pork proident nostrud qui ut. Shankle sint leberkas strip steak beef ribs nulla pastrami magna. Reprehenderit shoulder doner qui alcatra, sunt biltong officia ut pastrami brisket tenderloin andouille.</p>
                        <p>Fatback consectetur venison pig nulla. Exercitation t-bone turkey bresaola, eiusmod brisket beef ribs. Cow beef enim capicola. Tail leberkas dolor pariatur chuck biltong tri-tip turkey burgdoggen brisket. Drumstick anim veniam salami qui, alcatra culpa commodo laborum bresaola biltong cillum non burgdoggen.</p>
                        <p>Ham hock short loin swine, cow jowl ut mollit kielbasa dolor. Shankle velit beef ribs cupim nisi corned beef dolore jerky picanha bacon qui tenderloin est pork. Burgdoggen beef fugiat beef ribs ea pork belly sint duis cupim boudin et pig dolore. Aliqua laboris burgdoggen lorem andouille tail pig ball tip. Culpa pancetta in, tri-tip chicken pork laborum jowl pork belly beef ribs hamburger prosciutto enim et mollit. Dolor filet mignon occaecat, velit brisket andouille reprehenderit pork loin consectetur fugiat commodo. Pancetta mollit sunt picanha turducken prosciutto beef fatback cillum laborum pork belly fugiat short ribs kevin salami.</p>
                        <p>Pig leberkas fatback biltong incididunt ea spare ribs porchetta tempor cow frankfurter meatloaf. Ut duis landjaeger spare ribs eu. Dolore et ea, tenderloin drumstick velit incididunt eu pancetta aliqua. Shoulder in labore frankfurter. Venison irure enim pig, lorem esse boudin. Ut prosciutto t-bone shankle sunt pork doner chuck nulla culpa flank jowl landjaeger non.</p>
                        <p>Spicy jalapeno bacon ipsum dolor amet eu chuck lorem, venison commodo strip steak shankle capicola officia do cillum. Ground round quis ut, beef ullamco capicola short loin laborum sed doner pork loin cillum. Prosciutto cupidatat velit capicola aliqua. Incididunt strip steak leberkas meatloaf venison magna biltong sunt ham hock t-bone ham dolore flank pork chop irure.</p>
                        <p>Cillum ad kevin ribeye. Est mollit jerky, tri-tip rump sed flank qui ut pork belly tongue pastrami consequat short ribs aliqua. Irure jerky kielbasa, pork belly laboris lorem picanha eiusmod tenderloin shoulder chicken incididunt do pig landjaeger. Anim ut elit nulla quis sint capicola, tail meatball in. Pork proident nostrud qui ut. Shankle sint leberkas strip steak beef ribs nulla pastrami magna. Reprehenderit shoulder doner qui alcatra, sunt biltong officia ut pastrami brisket tenderloin andouille.</p>
                        <p>Fatback consectetur venison pig nulla. Exercitation t-bone turkey bresaola, eiusmod brisket beef ribs. Cow beef enim capicola. Tail leberkas dolor pariatur chuck biltong tri-tip turkey burgdoggen brisket. Drumstick anim veniam salami qui, alcatra culpa commodo laborum bresaola biltong cillum non burgdoggen.</p>
                        <p>Ham hock short loin swine, cow jowl ut mollit kielbasa dolor. Shankle velit beef ribs cupim nisi corned beef dolore jerky picanha bacon qui tenderloin est pork. Burgdoggen beef fugiat beef ribs ea pork belly sint duis cupim boudin et pig dolore. Aliqua laboris burgdoggen lorem andouille tail pig ball tip. Culpa pancetta in, tri-tip chicken pork laborum jowl pork belly beef ribs hamburger prosciutto enim et mollit. Dolor filet mignon occaecat, velit brisket andouille reprehenderit pork loin consectetur fugiat commodo. Pancetta mollit sunt picanha turducken prosciutto beef fatback cillum laborum pork belly fugiat short ribs kevin salami.</p>
                        <p>Pig leberkas fatback biltong incididunt ea spare ribs porchetta tempor cow frankfurter meatloaf. Ut duis landjaeger spare ribs eu. Dolore et ea, tenderloin drumstick velit incididunt eu pancetta aliqua. Shoulder in labore frankfurter. Venison irure enim pig, lorem esse boudin. Ut prosciutto t-bone shankle sunt pork doner chuck nulla culpa flank jowl landjaeger non.</p>
                        <p>Spicy jalapeno bacon ipsum dolor amet eu chuck lorem, venison commodo strip steak shankle capicola officia do cillum. Ground round quis ut, beef ullamco capicola short loin laborum sed doner pork loin cillum. Prosciutto cupidatat velit capicola aliqua. Incididunt strip steak leberkas meatloaf venison magna biltong sunt ham hock t-bone ham dolore flank pork chop irure.</p>
                        <p>Cillum ad kevin ribeye. Est mollit jerky, tri-tip rump sed flank qui ut pork belly tongue pastrami consequat short ribs aliqua. Irure jerky kielbasa, pork belly laboris lorem picanha eiusmod tenderloin shoulder chicken incididunt do pig landjaeger. Anim ut elit nulla quis sint capicola, tail meatball in. Pork proident nostrud qui ut. Shankle sint leberkas strip steak beef ribs nulla pastrami magna. Reprehenderit shoulder doner qui alcatra, sunt biltong officia ut pastrami brisket tenderloin andouille.</p>
                        <p>Fatback consectetur venison pig nulla. Exercitation t-bone turkey bresaola, eiusmod brisket beef ribs. Cow beef enim capicola. Tail leberkas dolor pariatur chuck biltong tri-tip turkey burgdoggen brisket. Drumstick anim veniam salami qui, alcatra culpa commodo laborum bresaola biltong cillum non burgdoggen.</p>
                        <p>Ham hock short loin swine, cow jowl ut mollit kielbasa dolor. Shankle velit beef ribs cupim nisi corned beef dolore jerky picanha bacon qui tenderloin est pork. Burgdoggen beef fugiat beef ribs ea pork belly sint duis cupim boudin et pig dolore. Aliqua laboris burgdoggen lorem andouille tail pig ball tip. Culpa pancetta in, tri-tip chicken pork laborum jowl pork belly beef ribs hamburger prosciutto enim et mollit. Dolor filet mignon occaecat, velit brisket andouille reprehenderit pork loin consectetur fugiat commodo. Pancetta mollit sunt picanha turducken prosciutto beef fatback cillum laborum pork belly fugiat short ribs kevin salami.</p>
                        <p>Pig leberkas fatback biltong incididunt ea spare ribs porchetta tempor cow frankfurter meatloaf. Ut duis landjaeger spare ribs eu. Dolore et ea, tenderloin drumstick velit incididunt eu pancetta aliqua. Shoulder in labore frankfurter. Venison irure enim pig, lorem esse boudin. Ut prosciutto t-bone shankle sunt pork doner chuck nulla culpa flank jowl landjaeger non.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section fourth-section fp-auto-height-responsive">
        <?php
        $params = array(
            'orderby'   => 'order ASC', 
            'limit'     => 5,
        ); 
        $coaches        = pods('coach', $params);
        $i = 0;
        ?>
<!-- subdesktop goes here -->
        <div class="fourth-section-desktop">
            <div class="fourth-section-slider">
                <?php while($coaches->fetch()) : ?>
                <?php $i++; ?>
                <div class="fourth-section-slider-slide slide slide-<?php echo $i; ?> <?php echo $coaches->field('permalink'); ?>" data-slide="<?php echo $i - 1; ?>" data-coach="<?php echo $coaches->field('permalink'); ?>">
                    <div class="coach-details-desktop">
                        <div class="coach-details-left subsection"></div>
                        <div class="coach-details-right subsection">
                            <div class="coach-details-big-pic"></div>
                        </div>
                        <div class="coach-details-main-content">
                            <h2><?php echo $coaches->field('name'); ?></h2>
                            <div><?php echo $coaches->display('short_text') ?></div>
                            <a class="about-info-button" data-coach="<?php echo $coaches->field('permalink'); ?>">More information</a>
                            <script type="text/javascript">
                                $('.fourth-section .about-info-button, .subdesktop-coach-box, .subdesktop-coach-box-inner, .subdesktop-coach-box-hover-shade, .subdesktop-coach-box-hover-button').click(function(){
                                    var coach = $(this).attr('data-coach');
                                    $('.fourth-section-modal.'+coach).fadeIn('slow');
                                });
                            </script>
                        </div>
                        <div class="coach-details-social-buttons">
                            <a href="<?php echo $coaches->field('facebook_link'); ?>" class="facebook" target="_blank"></a>
                            <a href="<?php echo $coaches->field('instagram_link'); ?>" class="instagram" target="_blank"></a>
                            <a href="<?php echo $coaches->field('youtube_link'); ?>" class="youtube" target="_blank"></a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php $coaches->reset(); ?>
            </div>
            <div class="fourth-section-coach-picker">
                <?php $i = 0; ?>
                <?php while($coaches->fetch()) : ?>
                <?php $i++; ?>
                <div class="coach-picker-box coach-picker-box-<?php echo $i; ?>"><a class="coach-<?php echo $i; ?> coach-navigation-button <?php echo $coaches->field('permalink'); ?> <?php if($i==1) echo 'active'; ?>" data-slide="<?php echo $i  - 1; ?>" data-coach-number="<?php echo $i; ?>"></a></div>
                <?php endwhile; ?>
                <script type="text/javascript">
                    $('.fourth-section-coach-picker .coach-navigation-button').click(function(){
                        var slide_number = $(this).attr('data-slide');
                        var coach_number = $(this).attr('data-coach-number');
                        $.fn.fullpage.moveTo('fourth', slide_number);
                        $('.fourth-section-coach-picker .coach-navigation-button').removeClass('active');
                        $('.fourth-section-coach-picker .coach-' + coach_number).addClass('active');
                    });
                </script>
            </div>
            <?php $coaches->reset(); ?>
            <?php $i = 0; ?>
            <?php while($coaches->fetch()) : ?>
            <?php $badge        = $coaches->field('badge'); ?>
            <?php $badge_url    = pods_image_url($badge, null); ?>
            <?php $i++; ?>
            <div class="fourth-section-modal <?php echo $coaches->field('permalink'); ?>">
                <a class="close-bttn"></a>
                <div class="fourth-section-modal-inner" style="background-image: url('<?php echo $badge_url; ?>')">
                    <h3><?php echo $coaches->field('name'); ?></h3>
                    <div class="fourth-section-modal-text">
                        <div class="fourth-section-modal-text-inner"><?php echo $coaches->display('text') ?></div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <script type="text/javascript">
                $('.fourth-section-modal .close-bttn').click(function(){
                    $('.fourth-section-modal').fadeOut('slow');
                });
            </script>
        </div>
    </div>
    <div class="section fifth-section fp-auto-height-responsive">
        <div class="fifth-section-desktop">
            <?php
            $today = date("Y-m-d");   
            $params = array(
                'orderby'   => 'date_start ASC', 
                'limit'     => 5,
                'where'     => 'active = 1 AND  date_start >= "$today"' 
            );

            $workshops = new pods('workshop', $params);
            $i = 0;
            ?>
            <?php while ( $workshops->fetch() ) : ?>
            <?php $i++; ?>
            <div class="fifth-section-slider-slide slide slide-<?php echo $i; ?>">
                <div class="fifth-section-left subsection">
                    <div class="fifth-section-left-inner">
                        <h2><?php echo $workshops->field('name'); ?></h2>
                        <p class="subtitle"><?php echo $workshops->field('subtitle'); ?></p>
                        <div class="text-and-calendar">
                            <p class="text"><?php echo $workshops->field('excerpt'); ?></p>
                            <div class="calendar">
                                <?php $dates = get_workshop_dates($workshops->field('date_start'), $workshops->field('date_end')); ?>
                                <div class="calendar-upper">
                                    <div class="day"><?php echo $dates['start_day']; ?></div>
                                    <div class="month"><?php echo $dates['start_month']; ?></div>
                                </div>
                                <div class="calendar-lower">
                                    <div class="day"><?php echo $dates['end_day']; ?></div>
                                    <div class="month"><?php echo $dates['end_month']; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="coaches">
                            <?php $coaches            = $workshops->field('coaches'); ?>
                            <?php $i = 0; ?>
                            <?php foreach ($coaches as $coach_a): ?>
                            <?php 
                                $coach              = pods('coach', $coach_a['id']);
                                $coach_badge        = $coach->field('badge');
                                $coach_badge_url    = pods_image_url($coach_badge, null);
                                $coach_first_name   = $coach->field('first_name');
                                $i++;
                            ?>
                            <div class="coach coach-<?php echo $i; ?>">
                                <div class="coach-image">
                                    <img src="<?php echo $coach_badge_url; ?>" />
                                </div>
                                <div class="coach-name"><?php echo $coach_first_name; ?></div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <a class="about-info-button" href="<?php echo site_url('workshop/'.$workshops->field('permalink')); ?>">More information</a>
                    </div>
                </div>
                <div class="fifth-section-right subsection">
                    <?php $image        = $workshops->field('image_secondary'); ?>
                    <?php $image_url    = pods_image_url($image, null); ?>
                    <div class="workshop-details-big-picture workshop-details-big-picture-1" style="background-image: url('<?php echo $image_url; ?>');"></div>
                </div>
            </div>
            <?php endwhile; ?>
            <div class="fifth-section-navigation">
                <?php $i = 0; ?>
                <?php $workshops->reset(); ?>
                <?php while ( $workshops->fetch() ) : ?>
                <a class="fifth-section-nav-item fifth-section-nav-item-<?php echo $i + 1; ?> <?php echo($i == 0 ? 'active' : ''); ?>" data-slide="<?php echo $i; ?>"><?php echo $workshops->field('name'); ?></a>
                <?php $i++; ?>
                <?php endwhile; ?>
            </div>
            <script type="text/javascript">
                $('.fifth-section-navigation .fifth-section-nav-item').click(function(){
                    var slide_number = $(this).attr('data-slide');
                    $.fn.fullpage.moveTo('fifth', slide_number);
                    $('.fifth-section-navigation .fifth-section-nav-item').removeClass('active');
                    $(this).addClass('active');
                });
            </script>
        </div>
        <?php $workshops->reset(); ?>
        <?php $i = 0; ?>
        <div class="fifth-section-subdesktop">
            <div class="fifth-section-subdesktop-heading">
                <h2>Upcoming<br/>workshops</h2>
            </div>
            <div class="fifth-section-subdesktop-workshops">
                <?php while ( $workshops->fetch() ) : ?>
                <?php $i++; ?>
                <div class="workshop-box workshop-box-1">
                    <h3><?php echo $workshops->field('name'); ?></h3>
                    <div class="text-and-calendar">
                        <div class="text"><?php echo $workshops->field('excerpt'); ?></div>
                        <div class="calendar">
                            <div class="calendar-upper">
                                <div class="day"><?php echo $dates['start_day']; ?></div>
                                <div class="month"><?php echo $dates['start_month']; ?></div>
                            </div>
                            <div class="calendar-lower">
                                <div class="day"><?php echo $dates['end_day']; ?></div>
                                <div class="month"><?php echo $dates['end_month']; ?></div>
                            </div>
                        </div>
                    </div>
                    <a class="about-info-button" href="<?php echo site_url('workshop/'.$workshops->field('permalink')); ?>">More information</a>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors:['first', 'second', 'third', 'fourth', 'fifth'],
        responsiveWidth: 992,
        normalScrollElements: '.third-section-modal-text-inner, .fourth-section-modal, .first-section-opt-in-modal'
    });
});
$(window).load(function(){
});
</script>

<?php get_footer('front');