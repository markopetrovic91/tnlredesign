        </div>
        <?php wp_footer(); ?> 
    </body>
<script>
$(document).ready(function(){

    $("#infusion-form-optin").validate({
        rules: {
            inf_field_FirstName: "required",
            inf_field_Email: {
                required: true,
                email: true
            }
        }
    });               
    $('#infusion-form-optin button').click(function(){
        if($("#infusion-form-optin").valid()){
            console.log("valid");
        }else{
            return false;
        }
    });
    secondSectionRes();
    $( window ).resize(function() {
        secondSectionRes();
    });
    function secondSectionRes(){
        var sirina = 0;
        $('.second-section .workshop-navigation-button').each(function() {
            sirina += $(this).outerWidth(true);     
        }); 
        var sirinaVise = sirina + 4;
        if (!$('.second-section-slider .workshop-navigation-wrapper').hasClass('morethan')) {
            $('.second-section-slider .workshop-navigation-wrapper').css('width', sirinaVise + 'px');
            $('.second-section .workshop-navigation-wrapper').css('margin-left', -(sirinaVise / 2) + 'px');
        }else{
            $('.second-section-slider .workshop-navigation-wrapper').css({'width': 615 + 'px', 'margin-left': -308 + 'px'});
        } 
    }
});

</script>
</html>