<!DOCTYPE html>
<html>
    <head>
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf8" />
        <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" type="image/x-icon" />
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cantata+One" rel="stylesheet">
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jquery.fullPage.css" />
        <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('stylesheet_url');?>" />
        <!-- JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/jquery.fullPage.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.validate.min.js"></script>
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class($class); ?>>
        <div id="front-page-wrapper" class="page-wrapper">
            <nav id="main-navigation-desktop" class="main-navigation">
                <div class="logo"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="The Natural Lifestyles" /></a></div>
                <!-- <ul class="navigation-inner log-in-out">
                    <li><a href="#">Login</a></li>
                </ul> -->
                <!-- <ul class="navigation-inner profile-links">
                    <li><a href="#">My account</a></li>
                    <li><a href="#">My profile</a></li>
                </ul> -->
                <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'navigation-inner standard')); ?>
            </nav>
            <nav id="main-navigation-subdesktop" class="main-navigation">
                <div class="logo"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="The Natural Lifestyles" /></a></div>
                <div class="navigation-inner-subdesktop-wrapper">
                    <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'navigation-inner standard')); ?>
                    <div class="subdesktop-nav-bottom">
                        <ul class="navigation-inner social-links">
                            <?php $tnl_social_links = new Pod('tnl_social_links'); ?>
                            <li><a href="<?php echo $tnl_social_links->field('facebook_link'); ?>" class="fb" target="_blank"></a></li>
                            <li><a href="<?php echo $tnl_social_links->field('instagram_link'); ?>" class="in" target="_blank"></a></li>
                            <li><a href="<?php echo $tnl_social_links->field('youtube_link'); ?>" class="yt" target="_blank"></a></li>
                        </ul>
                        <!-- <ul class="navigation-inner log-in-out">
                            <li><a href="#">Login</a></li>
                        </ul> -->
                        <!-- <ul class="navigation-inner profile-links">
                            <li><a href="#">My account</a></li>
                            <li><a href="#">My profile</a></li>
                        </ul> -->
                    </div>
                </div>
                <a class="menu-button closed">Menu</a>
                <script type="text/javascript">
                $('.menu-button').click(function(){
                    if($('.menu-button').hasClass('closed')) {
                        $('.navigation-inner-subdesktop-wrapper').animate({
                            height: 'toggle'
                        }, {
                            complete: function() {
                                $('.menu-button').addClass('open').removeClass('closed');
                            }
                        },
                        350);
                    } else if ($('.menu-button').hasClass('open')) {
                        $('.navigation-inner-subdesktop-wrapper').animate({
                            height: 'toggle'
                        }, {
                        complete: function() {
                            $('.menu-button').addClass('closed').removeClass('open');
                        }
                        },
                        350);
                    } else {
                        alert('Something went wrong!');
                    }
                });
                </script>
            </nav>
            