<?php /* Template name: About us */ ?>

<?php get_header('front'); ?>

<div id="fullpage" class="about-page-wrapper">
    <div class="section slider-section first-section">
    	<p class="about-big-text">If you’re a single guy struggling to connect with the women you want, please know this...</p>
    	<p>The reason for your lack of success isn’t because you’re not using the latest “PUA” tactics, nor is it because you’re don’t have what society deems attractive to women…</p>
    	<p>The truth is...</p>
    	<div class="scroll-down-icon"></div>
    </div>
    <div class="section slider-section second-section">
    	<p class="about-big-text">The success you desperately crave is blocked by something much simpler to solve.</p>
    	<div class="about-second-separator"></div>
    	<p class="about-big-text">So here’s the big question:</p>
    	<p>Do you want to waste years, money, your time and energy<br />Trying to impress women with success and money, or grabbing the next dating gimmick, hoping for an edge…</p>
    	<p>Or are you finally ready to make significant change and get the women you want and deserve?</p>
    	<div class="scroll-down-icon"></div>
    </div>
    <div id="third-section" class="section fp-normal-scroll third-section">
    	<div id="third-section-inner">
	    	<div class="third-section-1">
	    		<div class="third-section-1-img third-section-img">
	    			
    			</div>
	    		<div class="third-section-1-content third-section-content">
	    			<div class="content-inner">
	    				<p>My name is James Marshall, and I’m the founder and CEO of The Natural Lifestyles: The only dating coaching company on the world that focuses on becoming “Natural” with women. Since 2008 The Natural Lifestyles has been a pioneering world leader in natural seduction, inner change and lifestyle design. Thousands of men have smashed their dating, sex, love and lifestyle goals using our methods and philosophy and now it’s your chance.</p>
	    				<p class="about-big-text">But does it mean to be “Natural” with women?</p>
	    				<p>Throughout history there have been men who were “Naturals”. Casanovas, Lotharios, men who were able to attract beautiful women time and time again. And they weren’t necessarily handsome Greek Gods with chiseled bodies, nor were they rich tycoons with millions of dollars to splash.</p>
	    			</div>
	    		</div>
	    		<div class="third-section-1-img third-section-img">
	    			
    			</div>
	    	</div>
	    	<div class="third-section-2">
	    		<div class="third-section-2-img third-section-img">
	    			
	    		</div>
	    		<div class="third-section-2-content third-section-content">
	    			<div class="content-inner">
	    				<p>However, they understood women on an intimate level that had the world’s most coveted women falling head over heels with them, time and time again.</p>
	    				<p>I myself didn’t start out natural with women. In my early years, I was a flute playing, opera obsessed nerd who got constantly bullied by the jocks, and hardly any girls paid attention to me.</p>
	    				<p>I went through a long journey of self discovery which took me to studying Kung Fu in Shaolin temple, touring Australia in a funk band, studying eastern philosophies, sexual practices and healing methods. All the while learning about women, building social circles, changing my psyche, healing old pain and daily anxiety and gradually discovering my potential.</p>
	    				<p>And when I discovered the underground “pickup” scene, I threw myself deep into the art of seduction, going through the painful trial and error process to get the women I wanted. For two years I dedicated myself full time to approaching seducing and deconstructing the science and art of spontaneous natural seduction.</p>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="third-section-3">
    			<p>Eventually, I found myself joining up with a crew of 3 other “Naturals” who all had distinctly different styles with women. From blindingly fast rapid escalations in glamour clubs, to low energy, almost hypnotic seductions.</p>
    			<p>Rolling with these guys night after night I closely watched their movements and thinking processes. Amongst all the differences I saw the unifying principles at work behind each unique seduction.</p>
    			<p>I took these principles, which eventually I developed into the</p>
    			<p class="about-big-text">5 Principles</p>
	    	</div>
	    	<div class="third-section-4">
	    		<div class="third-section-4-content third-section-content">
	    			<div class="content-inner">
		    			<p>and started teaching the early students of TNL a rough, unpolished version of them… but even then it worked, and it worked well. Over the years TNL’s coaches and I have continued to innovate, experiment and pass on new yet timeless methods for dating and sexual success.</p>
		    			<p>It didn’t matter what race, height or age they were… if they used the principles, they became uniquely capable with women, and were able to pull off seductions they’d only dreamed of in the past.</p>
		    			<p class="about-big-text">But becoming “Natural” is just a fraction of what we teach our clients</p>
		    			<p>The fact is that within the seduction coaching industry, there is a culture of immature masculinity: where men are only competing to rack up the number of girls they’ve slept with, without using it as a tool to deepen their understanding of themselves, and to truly fulfill their potential as a man in the 21st century.</p>
	    			</div>
	    		</div>
	    		<div class="third-section-4-img third-section-img">
	    			
    			</div>
	    	</div>
	    	<div class="third-section-5">
	    		<div class="third-section-5-img third-section-img">
	    			
    			</div>
	    		<div class="third-section-5-content third-section-content">
	    			<div class="content-inner">
	    				<p><strong>There’s no shame here:</strong> if you’re looking to sleep with dozens of smoking hot girls before finding someone to get into a relationship with, we will show you the way.</p>
	    				<p>And if you’re looking to just find one quality woman to have a great relationship with, we’ll show you how to do that as well.</p>
	    				<p>On a live TNL program, our aim is to create <strong>a safe space to finally open up and grow in this crucial area of your life.</strong></p>
	    				<p>Men who find us often come from conservative or religious backgrounds that have led to them hiding their sexuality, and pushing away these natural human desires.</p>
	    			</div>
	    		</div>
	    		<div class="third-section-5-img third-section-img">
	    			
    			</div>
	    	</div>
	    	<div class="third-section-6">
	    		<div class="third-section-6-content third-section-content">
	    			<div class="content-inner">
	    				<p>Guys who are analytical by nature or habit and often haven’t been able to yet unlock their potential for flowing, fun and sexy interactions with women, especially ones they’ve just met.</p>
	    				<p>We work with our clients to get out of their heads, trying to plan their next move and instead learn the frameworks of good sexual communication and then how to let go and drop into relaxed spontaneous flow. We encourage our clients to open up their blocked emotions, clear their minds and direct their will and desire clearly without the need to be an arrogant prick.</p>
	    				<p><strong>There is no culture of competition or judgmental attitude on our programs:</strong> The men who are on the same program as you have been screened to be individuals whose intentions with women are positive, and from a place of authenticity and intimacy.</p>
	    			</div>
	    		</div>
	    		<div class="third-section-6-img third-section-img">
	    			
    			</div>
	    	</div>
	    	<div class="third-section-7">
    			<p class="about-big-text">If you’re reading this, you are most likely NOT satisfied with your dating life</p>
				<p>Whether you’ve just gotten out of a toxic relationship, a harsh divorce, or just been separated from women and feminine energy for too long, we at The Natural Lifestyles will show you the <strong>way to connect with your Inner Natural.</strong></p>
	    	</div>
	    	<div class="third-section-8">
	    		<div class="third-section-8-img third-section-img">
	    			
    			</div>
	    		<div class="third-section-8-content third-section-content">
	    			<div class="content-inner">
	    				<p><strong>Remember: What one man can do, another man can do as well.</strong>  I myself did not grow up natural with women. In my own seduction journey, I went from geeky flute-playing nerd to being a low energy, relaxed seducer through painful trial and error, and being lucky enough to stumble upon and become friends with several other naturals who showed me the ropes.</p>
	    				<p>Today, you don’t have to go through that struggle. We have taken the guesswork out of the equation for you, and we can show you the steps to becoming the natural you want to be, and to have the women that you dream of.</p>
	    			</div>
	    		</div>
	    		<div class="third-section-8-img third-section-img">
	    			
    			</div>
	    	</div>
	    	<div class="third-section-9">
    			<div class="third-section-9-title">
	    			<p>So here's what to do now if you want to</p>
	    			<p class="about-big-text">unlock your inner natural</p>
    			</div>
    			<div class="third-section-9-boxes">
    				<div class="third-section-9-box third-section-9-box-1">
    					<h3>Online programs</h3>
    					<p>Online paid programs and products, ranking from beginner foundations to advanced seductive frameworks</p>
    					<a href="#">Click here</a>
    				</div>
    				<div class="third-section-9-box third-section-9-box-2">
    					<h3>Skype coaching</h3>
    					<p>Online 1 on 1 Skype coaching sessions. Schedule a sessions with one of our coaches</p>
    					<a href="#">Click here</a>
    				</div>
    				<div class="third-section-9-box third-section-9-box-3">
    					<h3>Exclusive live coaching</h3>
    					<p>Highly customized live 4, 7 & 10 day programs worldwide for the ultimate in fast effective results</p>
    					<a href="#">Click here</a>
    				</div>
    			</div>
	    	</div>
	    	<div class="third-section-10">
	    		<div class="third-section-10-img third-section-img">
	    			
    			</div>
	    		<div class="third-section-10-content third-section-content">
	    			<div class="content-inner">
		    			<p>Our live programs sell out months in advance, and range from our infamous 10 day “Euro Tour” to intensive mini-residentials and week long residential programs, held in Europe, USA and Australia.</p>
	    				<p>We only teach <strong>70-100 men per year live</strong>, and have a strict 2:1 student-instructor ratio in order to create massive change in our student’s lives.</p>
	    				<p>There’s a sea of lost men in this world who continually struggle with women, and never reach their full potential of what they could be if only they had the mentors and tools to develop themselves.</p>
	    				<p>Do not be one of them.</p>
	    				<p>Just a heads up that you don’t have forever to work with me. I’ve been coaching men for nearly a decade now, and if you want me to personally help you with your dating problems you’ll need to get moving, as I plan on shifting into different ventures over the next few years.</p>
	    			</div>
	    		</div>
	    		<div class="third-section-10-img third-section-img">
	    			
    			</div>
	    	</div>
	    	<div class="third-section-11">
	    		<p class="logo"><img src="<?php bloginfo('template_directory'); ?>/img/about_11_tnl_logo_mobile.png" alt="The Natural Lifestyles" /></p>
    			<p>Join me and the world's greatest team of seduction, internal power and baller lifestyle design coaches - Liam McRae, Andrew Holiday, Tony Solo and Shae Mathews this year to smash your limitations and achive your true potential with women, confidence and lifestyle.</p>
    			<!-- <p>If you feel in your gut that it’s time to make a change in your mindsets, actions and that now is the time to move hard towards what you’ve always wanted, do not let this moment pass without either applying for a program, or writing in an inquiry to find out more.</p> -->
    			<a href="#">Click here to view our live programs</a>
	    	</div>
    	</div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors:['first', 'second', 'third'],
        responsiveWidth: 768,
        scrollBar: true,
        hybrid:true,
    	fitToSection: false
    });
});
$(window).load(function(){
});
</script>

<?php get_footer();