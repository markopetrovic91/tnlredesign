<?php /* Template name: Products */ ?>

<?php get_header(); ?>

<?php
$params     = array('limit' => -1,); 
$products   = pods('product', $params);
?>
<div class="products-category-header-wrapper">
    <div class="products-category-header-wrapper-inner">
        <div class="product-header-box-main">
            <div class="logo">
                <img class="logo-subdesktop" src="<?php bloginfo('template_directory'); ?>/img/logo_black_240.png" alt="The Natural Lifestyles" />
                <img class="logo-desktop" src="<?php bloginfo('template_directory'); ?>/img/logo_black_295.png" alt="The Natural Lifestyles" />
            </div>
            <div class="image">
                <img src="<?php echo content_url(); ?>/uploads/2017/05/products_header_img_mobile.png" alt="Products" />
            </div>
            <div class="text">
                <p>TNL has a range of digital training packages, with online products covering: The comperhensive methodology of Natural Seduction, meditation for social flow, massage & sexual mastery and productivity and lifestyle design. Check out our available products and information on upcoming launches and intakes for online courses below.</p>
            </div>
        </div>
        <div class="image-desktop">
            <img src="<?php echo content_url(); ?>/uploads/2017/05/products_header_img_mobile.png" alt="Products" />
        </div>
    </div>
</div>

<div class="products-category-wrapper inner-page-wrapper">
    <div class="products-category-wrapper-inner inner-page-wrapper-inner">
        <?php while($products->fetch()) : ?>
        <?php
        $name       = $products->field('name');
        $author     = $products->field('author');
        $image      = $products->field('image');
        $image_url  = pods_image_url($image, null);
        $text       = $products->field('short_text');
        $tags       = $products->field('tags');
        $price      = $products->field('price');
        $permalink  = $products->field('permalink');
        ?>
        <div class="product-box">
            <div class="product-box-inner">
                <div class="product-image-desktop">
                    <img src="<?php echo $image_url ?>" alt="<?php echo $name; ?>" />
                </div>
                <div class="product-box-main">
                    <!-- <div class="product-box-main"> -->
                        <h2><?php echo $name; ?></h2>
                        <div class="product-image-subdesktop">
                            <img src="<?php echo $image_url ?>" alt="<?php echo $name; ?>" />
                        </div>
                    <!-- </div> -->
                    <div class="author-box">
                        <?php $author_pod = pods('coach', $author['pod_item_id']); ?>
                        <?php
                        $author_image       = $author_pod->field('badge');
                        $author_image_url   = pods_image_url($author_image, null);
                        ?>
                        <div class="author-image"><img src="<?php echo $author_image_url; ?>" alt="<?php echo $author_pod->field('name'); ?>" /></div>
                        <div class="author-name">
                            <div class="author-label">Author</div>
                            <div class="author-name-inner"><?php echo $author_pod->field('name'); ?></div>
                        </div>
                    </div>
                    <div class="product-short-text">
                        <?php echo wpautop($text); ?>
                    </div>
                    <div class="product-tags">
                    <?php if($tags) : ?>
                        <?php foreach($tags as $tag) : ?>
                        <?php $tag_pod = pods('product_tag', $tag['pod_item_id']); ?>
                        <?php
                        $tag_icon       = $tag_pod->field('icon');
                        $tag_icon_url   = pods_image_url($tag_icon, null);
                        ?>
                        <div class="product-tag"><img src="<?php echo $tag_icon_url; ?>" alt="<?php echo $tag['name']; ?>" /><?php echo $tag['name']; ?></div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                    <div class="product-price">
                        <div><span>Price:</span> $<?php echo $price; ?></div>
                    </div>
                    <div class="product-single-page-bttn-wrapper">
                        <a class="product-single-page-bttn" href="<?php echo site_url('product/'.$permalink); ?>">More information</a>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
</div>
<div class="product-page-footer-wrapper">
    <div class="product-page-footer">
        <h2>Help</h2>
        <a href="#">Customer service contact</a>
        <a href="#">FAQ</a>
        <div class="product-page-icons">
            <img src="<?php bloginfo('template_directory'); ?>/img/ssl_icon.png" alt="SSL" />
            <img src="<?php bloginfo('template_directory'); ?>/img/buyer_protection_icon.png" alt="Buyer protection" />
            <img src="<?php bloginfo('template_directory'); ?>/img/money_back_icon.png" alt="30 days money back guarantee" />
        </div>
        <div class="copyright-and-cc">
            <div class="credit-cards"><img src="<?php bloginfo('template_directory'); ?>/img/cc_icon.png" alt="payment-options" /></div>
            <div class="copyright">Copyright © 2008-2017 The Natural Lifestyles Ltd. All Rights Reserved. User Agreement, Privacy, Cookies</div>
        </div>
    </div>
</div>

<?php get_footer();