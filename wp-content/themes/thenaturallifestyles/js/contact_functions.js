$(".contact-form").validate({
    rules: {
        first_name: "required",
        last_name: "required",
        email: {
            required: true,
            email: true
        },
        phone: "required",
        country: "required",
        question: "required"
    }
});

$(document).ready(function(){
    $('.contact-page-contact .contact-bttn').click(function(){
            $('.contact-modal').fadeIn();
    });
    $('.contact-modal .close-bttn').click(function(){
        $('.contact-modal').fadeOut();
    });
    $('.contact-form button').click(function(){
        if($(".contact-form").valid()){
            var first_name  = $('.contact-form input.first-name').val();
            var last_name   = $('.contact-form input.last-name').val();
            var email       = $('.contact-form input.email').val();
            var mobile      = $('.contact-form input.phone').val();
            var country     = $('.contact-form select.country option:checked').val();
            var question    = $('.contact-form textarea.question').val();

            var captcha     = $('.contact-form [name=g-recaptcha-response]').val();

            var data = {
                action:         'tnl_post_contact',
                security:       $('.contact-nonce').val(),
                first_name:     first_name,
                last_name:      last_name,
                email:          email,
                mobile:         mobile,
                country:        country,
                question:       question,
                captcha:        captcha
            };
            
            $.post(this_ajax_url, data, function(response) {
                var ajax_response = JSON.parse(response);
                if(ajax_response){
                    //console.log(ajax_response);
                    if(ajax_response['valid'] == false){
                        $('.contact-form .result-msg').empty();
                        $('<div class="error">Please check captcha field</div>').appendTo($('.contact-form .result-msg'));
                    }
                    else if(ajax_response['c_result'] == true){
                        $('.contact-form .result-msg').empty();
                        $('<div class="success">Form submitted successfuly</div>').appendTo($('.contact-form .result-msg'));
                    }
                    else{
                        $('.contact-form .result-msg').empty();
                        $('<div class="error">Error while submitting form. Please try again or contact support@thenaturallifestyles.com</div>').appendTo($('.inquiry-modal-step-3r .result-msg'));
                    }
                }
            });
            
            return false;
        }
    });
});