$("#inquiry-form-1").validate({
    rules: {
        first_name: "required",
        last_name: "required",
        email: {
            required: true,
            email: true
        },
        phone: "required",
        skype: "required"
    }
});

$("#inquiry-form-2").validate({
    rules: {
        age: "required",
        ethnicgroup: "required",
        student: "required",
        occupation: "required",
        coaching: "required",
        country: "required"
    }
});

$(document).ready(function(){
    $('.workshop-apply-button').click(function(){
        $('.inquiry-modal-navigation-item').removeClass('active');
        $('.inquiry-modal-navigation-item-1').addClass('active');
        $('.inquiry-modal-wrapper-outer').fadeIn();
        $('.inquiry-modal-step').hide();
        $('.inquiry-modal-step-1').show();
    });
    $('.inquiry-modal-close-bttn').click(function(){
        $('.inquiry-modal-wrapper-outer').fadeOut();
    });
    $('.inquiry-next-button-step-1').click(function(){
        if($("#inquiry-form-1").valid()){
            $('.inquiry-modal-step-1').fadeOut('fast', function(){
                $('.inquiry-modal-step-2').fadeIn();
                $('.inquiry-modal-navigation-item').removeClass('active');
                $('.inquiry-modal-navigation-item-2').addClass('active');
            });
            return false;
        }
    });
    $('.inquiry-next-button-step-2').click(function(){
        if($("#inquiry-form-2").valid()){
            var country         = $( "select[name=country] option:checked" ).val();
            var student         = $( "select[name=student] option:checked" ).val();
            var coaching        = $( "select[name=coaching] option:checked" ).val();
            var required_fields = true;
            var restricted_countries = [
                "Bulgaria",
                "Croatia",
                "Czech Republic (the)",
                "Hungary",
                "Lithuania",
                "Montenegro",
                "Romania",
                "Serbia",
                "Slovakia",
                "Slovenia",
                "Turkey",
                "Ukraine",
                "India",
                "Israel",
                "Estonia",
                "Ecuador",
                "Latvia",
                "Brazil",
                "Uruguay",
                "Argentina",
                "Paraguay",
                "Chile",
                "Bolivia (Plurinational State of)",
                "Peru",
                "Colombia",
                "Venezuela (Bolivarian Republic of)",
                "Guyana",
                "Panama",
                "Costa Rica",
                "Nicaragua",
                "Suriname",
                "French Guiana",
                "Dominican Republic (the)",
                "Puerto Rico",
                "Honduras",
                "Guatemala",
                "Belize",
                "El Salvador",
                "Pakistan",
                "Chad",
                "Lebanon",
                "Thailand",
                "Bangladesh",
                "Poland",
                "Philippines (the)",
            ];

            var restricted_country  = $.inArray(country,restricted_countries);
            var restricted_bool;
            var student_bool;
            var valid_candidate     = false;
            var skype               = false;

            if(restricted_country == -1){
                restricted_bool = false;
            }
            else{
                restricted_bool = true;
            }
            if(student == "I am"){
                student_bool = true;
            }
            else if(student == "I am not"){
                student_bool = false;
            }
            if(restricted_bool === false && student_bool === false){
                valid_candidate = true;
            }
            if(coaching == "Skype Coaching"){
                skype = true;
            }

            if(required_fields === false){
                alert('required_fields alert');
            }
            else if(skype === true){
                $('.inquiry-modal-step-2').fadeOut('fast', function(){
                    $('.inquiry-modal-step-3s').fadeIn();
                    $('.inquiry-modal-navigation-item').removeClass('active');
                    $('.inquiry-modal-navigation-item-3').addClass('active');
                });
            }
            else if(valid_candidate === false){
                $('.inquiry-modal-step-2').fadeOut('fast', function(){
                    $('.inquiry-modal-step-3p').fadeIn();
                    $('.inquiry-modal-navigation-item').removeClass('active');
                    $('.inquiry-modal-navigation-item-3').addClass('active');
                });
            }
            else if(valid_candidate === true){
                $('.inquiry-modal-step-2').fadeOut('fast', function(){
                    $('.inquiry-modal-step-3r').fadeIn();
                    $('.inquiry-modal-navigation-item').removeClass('active');
                    $('.inquiry-modal-navigation-item-3').addClass('active');
                });
            }

            return false;
        }
    });
    $("select.skype-coach").change(function() {
        var coach   = $( "select[name=skype_coach] option:checked" ).val();
        if(coach == undefined){
            coach = "liam";
        }
        $('.skype-coach-box.active').fadeOut('fast', function(){
            $('.skype-coach-box.active').removeClass('active');
            $('.skype-coach-box-' + coach).fadeIn();
            $('.skype-coach-box-' + coach).addClass('active');
        });
        $('.skype-coach-text.active').fadeOut('fast', function(){
            $('.skype-coach-text.active').removeClass('active');
            $('.skype-coach-text-' + coach).fadeIn();
            $('.skype-coach-text-' + coach).addClass('active');
        });
    });

});

$(document).on('click', '.inquiry-modal-step-3r-left button', function(){
    var first_name  = $('.inquiry-modal-wrapper-outer input.first-name').val();
    var last_name   = $('.inquiry-modal-wrapper-outer input.last-name').val();
    var email       = $('.inquiry-modal-wrapper-outer input.email').val();
    var mobile      = $('.inquiry-modal-wrapper-outer input.phone').val();
    var skype       = $('.inquiry-modal-wrapper-outer input.skype').val();
    var age         = $('.inquiry-modal-wrapper-outer select.age option:checked').val();
    var ethnicgroup = $('.inquiry-modal-wrapper-outer select.ethnicgroup option:checked').val();
    var student     = $('.inquiry-modal-wrapper-outer select.student option:checked').val();
    var occupation  = $('.inquiry-modal-wrapper-outer input.occupation').val();
    var coaching    = $('.inquiry-modal-wrapper-outer select.coaching option:checked').val();
    var country     = $('.inquiry-modal-wrapper-outer select.country option:checked').val();
    var question    = $('.inquiry-modal-wrapper-outer textarea.questions').val();

    var captcha     = $('.inquiry-modal-wrapper-outer  [name=g-recaptcha-response]').val();

    var data = {
        action:         'tnl_post_inquiry',
        security:       $('.inquiry-nonce').val(),
        first_name:     first_name,
        last_name:      last_name,
        email:          email,
        mobile:         mobile,
        skype:          skype,
        age:            age,
        ethnicgroup:    ethnicgroup,
        student:        student,
        occupation:     occupation,
        coaching:       coaching,
        country:        country,
        question:       question,
        captcha:        captcha
    };
    
    $.post(this_ajax_url, data, function(response) {
        var ajax_response = JSON.parse(response);
        if(ajax_response){
            //console.log(ajax_response);
            if(ajax_response['valid'] == false){
                $('.inquiry-modal-step-3r .result-msg').empty();
                $('<div class="error">Please check captcha field</div>').appendTo($('.inquiry-modal-step-3r .result-msg'));
            }
            else if(ajax_response['c_result'] == true){
                $('.inquiry-modal-step-3r .result-msg').empty();
                $('<div class="success">Form submitted successfuly</div>').appendTo($('.inquiry-modal-step-3r .result-msg'));
            }
            else{
                $('.inquiry-modal-step-3r .result-msg').empty();
                $('<div class="error">Error while submitting form. Please try again or contact support@thenaturallifestyles.com</div>').appendTo($('.inquiry-modal-step-3r .result-msg'));
            }
        }
    });

    return false;
});

var captchaload = function(){
    grecaptcha.render('inquiry-captcha', {
        'sitekey' : '6Lea_RYUAAAAAB3mq8KdXhhViEM-WABVFnxetud6'
    });
    grecaptcha.render('contact-captcha', {
        'sitekey' : '6Lea_RYUAAAAAB3mq8KdXhhViEM-WABVFnxetud6'
    });
};