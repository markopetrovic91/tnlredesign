$(document).ready(function(){
    var data = {
        action:         'tnl_get_blog_posts',
        security:       $('.blog-nonce').val(),
        filter:         '',
        page:           1,
    };
    
    $.post(this_ajax_url, data, function(response) {
        var ajax_response = JSON.parse(response);
        if(ajax_response){
            initial_populate_blog_posts(ajax_response['posts']);
            dynamic_pagination(ajax_response['pagination']['num_of_pages'], ajax_response['pagination']['current_page'], ajax_response['pagination']['filter']);
        }
    });
});

$(document).on('click', '.blog-filter', function(){
    var data = {
        action:         'tnl_get_blog_posts',
        security:       $('.blog-nonce').val(),
        filter:         $(this).attr('data-blog-filter'),
    };
    
    $.post(this_ajax_url, data, function(response) {
        var ajax_response = JSON.parse(response);
        if(ajax_response){
            populate_blog_posts(ajax_response['posts']);
            dynamic_pagination(ajax_response['pagination']['num_of_pages'], ajax_response['pagination']['current_page'], ajax_response['pagination']['filter']);
        }
    });
});

$(document).on('click', '.page-link.inactive', function(){
	var data = {
        action:         'tnl_get_blog_posts',
        security:       $('.blog-nonce').val(),
        filter: 		$(this).attr('data-filter'),
        page:           $(this).attr('data-page'),
    };
    
    $.post(this_ajax_url, data, function(response) {
        var ajax_response = JSON.parse(response);
        if(ajax_response){
            populate_blog_posts(ajax_response['posts']);
            dynamic_pagination(ajax_response['pagination']['num_of_pages'], ajax_response['pagination']['current_page'], ajax_response['pagination']['filter']);
        }
    });
});

$(document).on('click', '.post-box-blog a', function(){
    var post_id = $(this).attr('data-post-id');

    var data = {
        action:         'tnl_get_blog_post',
        security:       $('.blog-single-nonce').val(),
        id:             post_id,
    };
    
    $.post(this_ajax_url, data, function(response) {
        var ajax_response = JSON.parse(response);
        if(ajax_response){
            blog_modal_populate_and_show(ajax_response);
        }
    });

    return false;
});

$(document).on('click', '.post-box-vlog a, .post-box-video a', function(){
    var post_id = $(this).attr('data-post-id');

    var data = {
        action:         'tnl_get_video_post',
        security:       $('.blog-single-nonce').val(),
        id:             post_id,
    };
    
    $.post(this_ajax_url, data, function(response) {
        var ajax_response = JSON.parse(response);
        if(ajax_response){
            video_modal_populate_and_show(ajax_response);
            //alert('video');
        }
    });
0
    return false;
});

$(document).on('click', '.blog-modal-close-bttn', function(){
    $('.blog-modal-wrapper-outer').fadeOut();
});

$(document).on('click', '.video-modal-close-bttn', function(){
    $('.video-modal-wrapper-outer').fadeOut();
});

function populate_blog_posts(posts){
    var i = 1;
    var odder = '';
    $('.post-box').fadeOut('fast', function(){
        $('.post-box').remove();
        $.each(posts, function(index, post){
            if(i%2 == 0){
                odder = 'even';
            }
            else{
                odder = 'odd';
            }
            $('<div class="post-box ' + odder + ' post-box-' + post['type'][0]['slug'] + '"><div class="post-image"><a href="' + post['permalink'] + '" class="post-link" data-post-id="' + post['id'] + '"><img src="' + post['img'] + '" alt="' + post['title'] + '" /><div class="overlay"><span class="expand">OPEN</span></div></a></div><div class="post-inner-box clearfix"><a href="' + post['permalink'] + '" class="post-link" data-post-id="' + post['id'] + '"><h2>' + post['title'] + '</h2></a><div class="blog-badge blog-badge-' + post['type'][0]['slug'] + '"></div><div class="author-box"><div>Author</div><div class="blog-cat-author"><a href="' + post['author_url'] + '">' + post['author'] + '</a></div></div><div class="date-box">' + post['date'] + '</div></div></div>').appendTo('.blog-post-boxes');
            i++;
        });
    });
}

function initial_populate_blog_posts(posts){
    var i = 1;
    var odder = '';
    $.each(posts, function(index, post){
        if(i%2 == 0){
            odder = 'even';
        }
        else{
            odder = 'odd';
        }
        $('<div class="post-box ' + odder + ' post-box-' + post['type'][0]['slug'] + '"><div class="post-image"><a href="' + post['permalink'] + '" class="post-link" data-post-id="' + post['id'] + '"><img src="' + post['img'] + '" alt="' + post['title'] + '" /><div class="overlay"><span class="expand">OPEN</span></div></a></div><div class="post-inner-box clearfix"><a href="' + post['permalink'] + '" class="post-link" data-post-id="' + post['id'] + '"><h2>' + post['title'] + '</h2></a><div class="blog-badge blog-badge-' + post['type'][0]['slug'] + '"></div><div class="author-box"><div>Author</div><div class="blog-cat-author"><a href="' + post['author_url'] + '">' + post['author'] + '</a></div></div><div class="date-box">' + post['date'] + '</div></div></div>').appendTo('.blog-post-boxes');
        i++;
    });
}

function blog_modal_populate_and_show(post){
    // var post_social =   '<div class="blog-single-social">' +
    //                     '<p>&nbsp;</p>' +
    //                     '<p>Share on social networks</p>' +
    //                     '<div class="fb-like" data-href="' + post['full_url'] + '" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div> ' +
    //                     '<a class="twitter-share-button" href="https://twitter.com/share" data-size="large">Tweet</a>' +
    //                     '<div class="g-plusone" data-annotation="none"></div>' +
    //                     '</div>';
    var post_social = '';
    //var social_header   = $('.social-header').html();
    var social_header   =   '<!-- FB --><div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";  fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script><!-- FB end --><!-- Twitter --><script>window.twttr = (function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0],    t = window.twttr || {};  if (d.getElementById(id)) return t;  js = d.createElement(s);  js.id = id;  js.src = "https://platform.twitter.com/widgets.js";  fjs.parentNode.insertBefore(js, fjs);  t._e = [];  t.ready = function(f) {    t._e.push(f);  };  return t;}(document, "script", "twitter-wjs"));</script><!-- Twitter end -->';

    //var social_footer   = $('.social-footer').html();
    var social_footer   =   '<script src="https://apis.google.com/js/platform.js" async defer></script>';

    $('.social-header').empty();
    $('.social-footer').empty();
    $('.blog-modal-content-box-inner').empty();
    //$('<div class="post-image"><img src="' + post['img'] + '" /></div><div class="post-content ' + post['type'][0]['slug'] + '"><div class="post-title-box"><h2>' + post['post_title'] + '</h2><div class="date-box">' + post['post_date'] + '</div></div><div class="post-text">' + post['content'] + '</div></div><div class="post-subcontent-workshop">s</div>').appendTo('.blog-modal-content-box-inner');
    $('<div class="post-image" style="background-image: url('+post['img']+');"><a href="' + post['author_url'] + '"><div class="blog-single-author-box"><div class="author-label-name"><div class="author-label">Author</div><div class="author-name">' + post['author'] + '</div></div><div class="author-image '+ post['author_slug'] +'"></div></div></a></div><div class="post-content ' + post['type'][0]['slug'] + '"><div class="post-title-box"><a href="' + post['full_url'] + '"><h2>' + post['post_title'] + '</h2></a><div class="date-box">' + post['post_date'] + '</div></div><div class="post-text">' + post['content'] + ' ' + post_social + '</div></div>').appendTo('.blog-modal-content-box-inner');
    $('.social-header').html(social_header);
    $('.social-footer').html(social_footer);
    var twitter_url = 'https://twitter.com/share/tweet?text=' + post['full_url'];
    $('.fb-like').attr('data-href', post['full_url']);
    $('.twitter-share-button').attr('href', twitter_url);
    $('.blog-modal-wrapper-outer').fadeIn();
}

function video_modal_populate_and_show(post){
    $('.video-modal-content-box-inner-content').empty();
    //$('<div class="post-image"><iframe width="560" height="315" src="https://www.youtube.com/embed/'+ post['video_id'] +'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div><div class="post-content ' + post['type'][0]['slug'] + '"><div class="post-title-box"><a href="' + post['full_url'] + '"><h2>' + post['post_title'] + '</h2></a><div class="date-box">' + post['post_date'] + '</div></div><div class="post-text">' + post['content'] + ' ' + post_social + '</div></div>').appendTo('.video-modal-content-box-inner');
    $('<div class="post-image"><iframe width="855" height="480" src="https://www.youtube.com/embed/'+ post['video_id'] +'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div><div class="post-inner-box"><a href="' + post['permalink'] + '" class="post-link" data-post-id="' + post['id'] + '"><h2>' + post['post_title'] + '</h2></a><div class="blog-badge blog-badge-' + post['type'][0]['slug'] + '"></div><div class="author-box"><div>Author</div><div><a href="' + post['author_url'] + '">' + post['author'] + '</a></div></div><div class="date-box">' + post['date'] + '</div></div>').appendTo('.video-modal-content-box-inner-content');
    $('.video-modal-wrapper-outer').fadeIn();
}

function dynamic_pagination(num_of_pages, current_page, filter){
    current_page = parseInt(current_page);
    if(!$.isNumeric(current_page)){
        current_page = 1;
    }
    // 5 pages or under - no (...) before the last or first link
    var i = 0;
    var current_page_active = '';
    var prev = current_page - 1;
    if(prev < 1){
        prev = 1;
    }    
    var next = current_page + 1;
    if(next > num_of_pages){
        next = num_of_pages;
    }
    var first_active    = 'inactive'; 
    var last_active     = 'inactive';
    if(current_page == 1){
        first_active = 'active';
    }

    if(current_page == num_of_pages){
        last_active = 'active';
    }
    if(num_of_pages <= 5){
        $('.blog-pagination ul').empty();
        $('<li class="page-link first '+first_active+'" data-page="1" data-filter="'+filter+'">First</li>').appendTo('.blog-pagination ul');
        $('<li class="page-link previous '+first_active+'" data-page="'+prev+'" data-filter="'+filter+'">Prev</li>').appendTo('.blog-pagination ul');
        for(i = 1; i <= num_of_pages; i++){
            if(i == current_page){
                current_page_active = 'active';
            }
            else{
                current_page_active = 'inactive';
            }
            $('<li class="page-link number '+current_page_active+'" data-page="'+i+'" data-filter="'+filter+'">' + i +'</li>').appendTo('.blog-pagination ul');
        }
        $('<li class="page-link last '+last_active+'" data-page="'+num_of_pages+'" data-filter="'+filter+'">Last</li>').appendTo('.blog-pagination ul');
        $('<li class="page-link next '+last_active+'" data-page="'+next+'" data-filter="'+filter+'">Next</li>').appendTo('.blog-pagination ul');
    }
    // more than 5 pages - has (...) before last or first link
    else if(num_of_pages > 5){
        $('.blog-pagination ul').empty();
        var start_num = 1;
        if(current_page > 3){
            start_num = current_page - 2;
        }
        if(current_page >= (num_of_pages - 2)){
            start_num = num_of_pages - 4;
        }
        $('<li class="page-link first '+first_active+'" data-page="1" data-filter="'+filter+'">First</li>').appendTo('.blog-pagination ul');
        $('<li class="page-link previous '+first_active+'" data-page="'+prev+'" data-filter="'+filter+'">Prev</li>').appendTo('.blog-pagination ul');
        for(i = start_num; i <= start_num + 4; i++){
            if(i == current_page){
                current_page_active = 'active';
            }
            else{
                current_page_active = 'inactive';
            }
            $('<li class="page-link number '+current_page_active+'" data-page="'+i+'" data-filter="'+filter+'">' + i +'</li>').appendTo('.blog-pagination ul');
        }
        $('<li class="page-link last '+last_active+'" data-page="'+num_of_pages+'" data-filter="'+filter+'">Last</li>').appendTo('.blog-pagination ul');
        $('<li class="page-link next '+last_active+'" data-page="'+next+'" data-filter="'+filter+'">Next</li>').appendTo('.blog-pagination ul');
    }
}