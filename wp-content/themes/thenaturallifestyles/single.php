<?php get_header(); ?>
<!-- FB -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- FB end -->
<!-- Twitter -->
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>
<!-- Twitter end -->
<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post(); ?>
<div class="blog-single-wrapper inner-page-wrapper">
	<?php 
	$image          = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
	$img_src        = $image[0];
 	$author_link	= get_author_posts_url(get_the_author_meta('ID'))
	?>
    <div class="blog-single-banner-wrapper" style="background-image: url('<?php echo $img_src; ?>')">
    	<a href="<?php echo $author_link; ?>">
	        <div class="blog-single-author-box">
	        	<div class="author-label-name">
	        		<div class="author-label">Author</div>
	        		<div class="author-name"><?php echo get_the_author(); ?></div>
	        	</div>
                <?php $authorSlug = get_the_author(); ?>
                <div class="author-image <?php echo $authorSlug; ?>"></div>
	        </div>
        </a>
    </div>
    <div class="blog-single-wrapper-inner inner-page-wrapper-inner">
	    <?php 
	    	$catg	= get_the_category($post_id);
	    	$cat 	= $catg[0]->slug;
	    ?>
		<div class="post-content <?php echo $cat; ?>">
			<div class="post-title-box">
				<h2><?php echo get_the_title(); ?></h2>
				<div class="date-box"><?php echo get_the_date(); ?></div>
			</div>
			<div class="post-text">
				<?php the_content(); ?>
				<div class="blog-single-social">
					<p>&nbsp;</p>
					<p>Share on social networks</p>
					<div class="fb-like" data-href="<?php echo the_permalink(); ?>" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
					<a class="twitter-share-button" href="https://twitter.com/share" data-size="large">Tweet</a>
					<div class="g-plusone" data-annotation="none"></div>
				</div>
			</div>
		</div>
    </div>
</div>
<?php featured_webshop(); ?>
<!-- <div class="blog-single-workshop-wrapper inner-page-wrapper">
    <div class="workshop-box inner-page-wrapper-inner">
        <div class="workshop-image-wrapper ">
            <?php
                //$image              = $workshops->field('image_secondary');
                //$image_url          = pods_image_url($image, null);
                //$dates              = get_workshop_dates($workshops->field('date_start'), $workshops->field('date_end'));
            ?>
            <img src="<?php // echo site_url('wp-content/uploads/2016/10/budapest_workshop_pages.jpg'); ?>" alt="Budapest Residential" />
        </div>
        <div class="workshop-details-wrapper">
            <h2>Budapest residential</h2>
            <p class="subtitle">Some cool subtitle or subtities</p>
            <div class="text-and-calendar">
                <div class="text">Lorem ipsum, tinu dinu, ide milicija bale a gde si ti? Leto je, gde si ti, ne znam gde si ti, a to bih morala znati, o-je-je-je. Noc je vrela a meni hladno je. A meni stvarno hladno je.</div>
                <div class="calendar non-mobile">
                    <div class="calendar-upper">
                        <div class="day">date_start</div>
                        <div class="month">oct</div>
                    </div>
                    <div class="calendar-lower">
                        <div class="day">date_end</div>
                        <div class="month">oct</div>
                    </div>
                </div>
            </div>
            <div class="coaches-and-calendar">
                <div class="coaches">
                    <?php //$coaches            = $workshops->field('coaches'); ?>
                    <?php //$i = 0; ?>
                    <?php //foreach ($coaches as $coach_a): ?>
                    <?php 
                        //$coach              = pods('coach', $coach_a['id']);
                        //$coach_badge        = $coach->field('badge');
                        //$coach_badge_url    = pods_image_url($coach_badge, null);
                        //$coach_first_name   = $coach->field('first_name');
                        //$i++;
                    ?>
                    <div class="coach coach-james">
                        <div class="coach-image">
                            <img src="<?php // echo site_url('wp-content/uploads/2016/10/coach_badge_james_b_140.png'); ?>" />
                        </div>
                        <div class="coach-name">James</div>
                    </div>
                    <div class="coach coach-james">
                        <div class="coach-image">
                            <img src="<?php // echo site_url('wp-content/uploads/2016/10/coach_badge_james_b_140.png'); ?>" />
                        </div>
                        <div class="coach-name">James</div>
                    </div>
                    <?php //endforeach; ?>
                </div>
                <div class="calendar mobile">
                    <div class="calendar-upper">
                        <div class="day">24</div>
                        <div class="month">oct</div>
                    </div>
                    <div class="calendar-lower">
                        <div class="day">27</div>
                        <div class="month">oct</div>
                    </div>
                </div>
            </div>
            <a class="about-info-button" href="#">More information</a>
        </div>
    </div>
</div> -->
<?php endwhile; ?>
<?php endif; ?>  
<!-- G+ -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<!-- G+ end -->
<?php get_footer();