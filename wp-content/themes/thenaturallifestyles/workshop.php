<?php /* Template name: Workshop */ ?>

<?php get_header('contact'); ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/intlTelInput.min.js"></script>

<?php 
    $slug = pods_v_sanitized( 1, "url" );
    $workshop           = pods('workshop', $slug);
    $name               = $workshop->field('name');
    $text               = $workshop->field('text');
    $image              = $workshop->field('image_primary');
    $image_url          = pods_image_url($image, null);
    $title_big          = $workshop->field('title_big_letters');
    $title_small        = $workshop->field('title_small_letters');
    $spots_total        = $workshop->field('spots_total');
    $spots_available    = $workshop->field('spots_available');
    $coaches            = $workshop->field('coaches');
    $dates              = get_workshop_dates($workshop->field('date_start'), $workshop->field('date_end'));
?>

<div class="workshop-single-top-image-box" style="background-image: url('<?php echo $image_url; ?>');">
    <div class="workshop-single-top-image-box-inner">
        <div class="workshop-name-wrapper">
            <h2><?php echo $title_big; ?><span><?php echo $title_small; ?></span></h2>
        </div>
        <div class="workshop-date-wrapper">
            <div class="workshop-date-upper"><?php echo $dates['start_month']; ?></div>
            <div class="workshop-date-middle">
                <div class="workshop-date-middle-left"><?php echo $dates['start_day']; ?></div>
                <div class="workshop-date-middle-right"><?php echo $dates['end_day']; ?></div>
            </div>
            <div class="workshop-date-bottom"><?php echo $dates['end_month']; ?></div>
        </div>
    </div>
    <div class="scroll-down-icon"></div>
</div>
<div class="workshop-single-wrapper inner-page-wrapper">
    <div class="workshop-single-wrapper-inner inner-page-wrapper-inner">
        <div class="spots-and-coaches">
            <div class="spots">
                <p><?php echo $spots_available ?> spot(s) available</p>
                <div class="spots-dots spots-dots-subdesktop" style="width: <?php echo ($spots_total * 35 - 10) . 'px' ?>;">
                    <?php for($i = 0; $i < ($spots_total - $spots_available); $i++ ): ?>
                    <div class="spot spot-taken"></div>
                    <?php endfor; ?>
                    <?php for($i = 0; $i < $spots_available; $i++ ): ?>
                    <div class="spot spot-available"></div>
                    <?php endfor; ?>
                </div>
                <div class="spots-dots spots-dots-desktop" style="width: <?php echo ($spots_total * 35 - 10) . 'px' ?>;">
                    <?php for($i = 0; $i < ($spots_total - $spots_available); $i++ ): ?>
                    <div class="spot spot-taken"></div>
                    <?php endfor; ?>
                    <?php for($i = 0; $i < $spots_available; $i++ ): ?>
                    <div class="spot spot-available"></div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="coaches">
                <div class="coaches-inner coaches-inner-subdesktop" style="width: <?php echo (count($coaches) * 85 - 15) . 'px' ?>;">
                    <?php foreach ($coaches as $coach_a): ?>
                    <?php 
                        $coach              = pods('coach', $coach_a['id']);
                        $coach_badge        = $coach->field('badge');
                        $coach_badge_url    = pods_image_url($coach_badge, null);
                        $coach_first_name   = $coach->field('first_name');
                    ?>
                    <div class="coach-box">
                        <div class="coach-badge"><img src="<?php echo $coach_badge_url; ?>" /></div>
                        <div class="coach-first-name"><?php echo $coach_first_name; ?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="coaches-inner coaches-inner-desktop" style="width: <?php echo (count($coaches) * 110 - 20) . 'px' ?>;">
                    <?php foreach ($coaches as $coach_a): ?>
                    <?php 
                        $coach              = pods('coach', $coach_a['id']);
                        $coach_badge        = $coach->field('badge');
                        $coach_badge_url    = pods_image_url($coach_badge, null);
                        $coach_first_name   = $coach->field('first_name');
                    ?>
                    <div class="coach-box">
                        <div class="coach-badge"><img src="<?php echo $coach_badge_url; ?>" /></div>
                        <div class="coach-first-name"><?php echo $coach_first_name; ?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="workshop-single-text"><div class="excerpt"><?php echo $workshop->display('excerpt'); ?></div><?php echo $workshop->display('text'); ?></div>
    </div>
</div>
<div class="workshop-single-inquiry-wrapper">
    <div class="workshop-single-inquiry-wrapper-inner inner-page-wrapper-inner">
        <div class="shaun-badge">
            <img src="<?php bloginfo('template_directory'); ?>/img/shaun_badge_130_130.png" alt="Shaun Masters sales manager" />
        </div>
        <h2>Interested in training?</h2>
        <p>Or have another question or inquiry?</p>
        <p>Click on the button below and a TNL member will contact you shortly.</p>
        <a class="workshop-apply-button">Apply for a workshop</a>
    </div>
</div>

<?php include('inquiry_modal.php'); ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/inquiry_functions.js"></script>
<script src='https://www.google.com/recaptcha/api.js?onload=captchaload&render=explicit'></script>

<script type="text/javascript">
    $(document).ready(function() { fullscreenvideo(); });
    $(window).resize(function() { fullscreenvideo(); });
    function fullscreenvideo () {
        $('.workshop-single-top-image-box').height($(window).height() - $('#main-navigation-desktop').height());
        var marginTop = $('.workshop-single-top-image-box').height();
        $('.workshop-single-wrapper.inner-page-wrapper').css('margin-top', marginTop + "px");
    }
</script>

<?php get_footer();