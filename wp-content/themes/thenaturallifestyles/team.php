<?php /* Template name: Team */ ?>

<?php get_header('front'); ?>

<div id="fullpage">
    <div class="section members-section fourth-section fp-auto-height-responsive">
        <?php
        $params = array(
            'orderby'   => 'order ASC', 
            'limit'     => 5,
        ); 
        $coaches        = pods('coach', $params);
        $i = 0;
        ?>
        <div class="fourth-section-subdesktop members-section-desktop">
            <div class="fourth-section-subdesktop-inner members-section-subdesktop-inner">
                <?php while($coaches->fetch()) : ?>
                <?php $i++; ?>
                <?php $subdesktop_coach_image        = $coaches->field('subdesktop_home_page_slider_default'); ?>
                <?php $subdesktop_coach_image_url    = pods_image_url($subdesktop_coach_image, null); ?>
                <div class="subdesktop-coach-box" data-coach="<?php echo $coaches->field('permalink'); ?>">
                    <div class="subdesktop-coach-box-inner" style="background-image: url('<?php echo $subdesktop_coach_image_url; ?>') ;"></div>
                    <div class="subdesktop-coach-box-hover-shade" data-coach="<?php echo $coaches->field('permalink'); ?>"></div>
                    <a class="subdesktop-coach-box-hover-button" data-coach="<?php echo $coaches->field('permalink'); ?>">More information</a>
                </div>
                <script type="text/javascript">
                $('.subdesktop-coach-box, .subdesktop-coach-box-inner').click(function(){
                    var coach = $(this).attr('data-coach');
                    $('.subdesktop-coach-box-hover-shade').hide();
                    $('.subdesktop-coach-box-hover-button').hide();
                    $('.subdesktop-coach-box-hover-shade[data-coach=' + coach + ']').show();
                    $('.subdesktop-coach-box-hover-button[data-coach=' + coach + ']').show();
                });
                $('.subdesktop-coach-box-hover-shade, .subdesktop-coach-box-hover-button').click(function(){
                    var coach = $(this).attr('data-coach');
                    $('.fourth-section-modal.'+coach).fadeIn('slow', function(){
                        $('.subdesktop-coach-box-hover-shade').hide();
                        $('.subdesktop-coach-box-hover-button').hide();
                    });
                });
                </script>
                <?php endwhile; ?>
                <?php $coaches->reset(); ?>
                <?php $i = 0; ?>
                <?php while($coaches->fetch()) : ?>
                <?php $badge        = $coaches->field('badge'); ?>
                <?php $badge_url    = pods_image_url($badge, null); ?>
                <?php $i++; ?>
                <div class="fourth-section-modal members-section-modal <?php echo $coaches->field('permalink'); ?>">
                    <a class="close-bttn"></a>
                    <div class="fourth-section-modal-inner members-section-modal-inner" style="background-image: url('<?php echo $badge_url; ?>')">
                        <h3><?php echo $coaches->field('name'); ?></h3>
                        <div class="fourth-section-modal-text members-section-modal-text">
                            <div class="fourth-section-modal-text-inner members-section-modal-text-inner"><?php echo $coaches->display('text') ?></div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php $coaches->reset(); ?>
            </div>
        </div>
        <div class="fourth-section-desktop members-section-desktop">
            <div class="fourth-section-slider members-section-slider">
                <?php while($coaches->fetch()) : ?>
                <?php $i++; ?>
                <div class="fourth-section-slider-slide members-section-slider-slide slide slide-<?php echo $i; ?> <?php echo $coaches->field('permalink'); ?>" data-slide="<?php echo $i - 1; ?>" data-coach="<?php echo $coaches->field('permalink'); ?>">
                    <div class="coach-details-desktop">
                        <div class="coach-details-left subsection"></div>
                        <div class="coach-details-right subsection">
                            <div class="coach-details-big-pic"></div>
                        </div>
                        <div class="coach-details-main-content">
                            <h2><?php echo $coaches->field('name'); ?></h2>
                            <div><?php echo $coaches->display('short_text') ?></div>
                            <a class="about-info-button" data-coach="<?php echo $coaches->field('permalink'); ?>">More information</a>
                            <script type="text/javascript">
                                $('.fourth-section .about-info-button').click(function(){
                                    var coach = $(this).attr('data-coach');
                                    $('.fourth-section-modal.'+coach).fadeIn('slow');
                                });
                            </script>
                        </div>
                        <div class="coach-details-social-buttons">
                            <a href="<?php echo $coaches->field('facebook_link'); ?>" class="facebook" target="_blank"></a>
                            <a href="<?php echo $coaches->field('instagram_link'); ?>" class="instagram" target="_blank"></a>
                            <a href="<?php echo $coaches->field('youtube_link'); ?>" class="youtube" target="_blank"></a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php $coaches->reset(); ?>
            </div>
            <div class="fourth-section-coach-picker members-section-coach-picker">
                <?php $i = 0; ?>
                <?php while($coaches->fetch()) : ?>
                <?php $i++; ?>
                <div class="coach-picker-box coach-picker-box-<?php echo $i; ?>"><a class="coach-<?php echo $i; ?> coach-navigation-button <?php echo $coaches->field('permalink'); ?> <?php if($i==1) echo 'active'; ?>" data-slide="<?php echo $i  - 1; ?>" data-coach-number="<?php echo $i; ?>"></a></div>
                <?php endwhile; ?>
                <script type="text/javascript">
                    $('.fourth-section-coach-picker .coach-navigation-button').click(function(){
                        var slide_number = $(this).attr('data-slide');
                        var coach_number = $(this).attr('data-coach-number');
                        $.fn.fullpage.moveTo('members', slide_number);
                        $('.fourth-section-coach-picker .coach-navigation-button').removeClass('active');
                        $('.fourth-section-coach-picker .coach-' + coach_number).addClass('active');
                    });
                </script>
            </div>
            <?php $coaches->reset(); ?>
            <?php $i = 0; ?>
            <?php while($coaches->fetch()) : ?>
            <?php $badge        = $coaches->field('badge'); ?>
            <?php $badge_url    = pods_image_url($badge, null); ?>
            <?php $i++; ?>
            <div class="fourth-section-modal members-section-modal <?php echo $coaches->field('permalink'); ?>">
                <a class="close-bttn"></a>
                <div class="fourth-section-modal-inner members-section-modal-inner" style="background-image: url('<?php echo $badge_url; ?>')">
                    <h3><?php echo $coaches->field('name'); ?></h3>
                    <div class="fourth-section-modal-text members-section-modal-text">
                        <div class="fourth-section-modal-text-inner members-section-modal-text-inner"><?php echo $coaches->display('text') ?></div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <script type="text/javascript">
                $('.fourth-section-modal .close-bttn').click(function(){
                    $('.fourth-section-modal').fadeOut('slow');
                });
            </script>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors:['members'],
        responsiveWidth: 992,
        normalScrollElements: '.members-section-modal-text-inner'
    });
});
$(window).load(function(){
});
</script>

<?php get_footer(); ?>