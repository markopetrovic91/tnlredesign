<?php /* Template name: Workshops */ ?>

<?php get_header(); ?>

<?php
$today = date("Y-m-d");
$params = array(
    'orderby'   => 'order ASC', 
    'limit'     => 6,
    'where'     => 'active = 1 AND date_start >= '.$today 
); 
$workshops      = pods('workshop', $params);
?>

<div class="workshops-category-wrapper inner-page-wrapper">
    <div class="workshops-category-wrapper-inner">
        <?php while ( $workshops->fetch() ) : ?>
        <div class="workshop-box">
            <div class="workshop-image-wrapper">
                <?php
                    $image              = $workshops->field('image_secondary');
                    $image_url          = pods_image_url($image, 'workshop_cat_large');
                    $dates              = get_workshop_dates($workshops->field('date_start'), $workshops->field('date_end'));
                ?>
                <img src="<?php echo $image_url ?>" alt="Budapest Residential" />
            </div>
            <div class="workshop-details-wrapper">
                <h2><?php echo $workshops->display('name'); ?></h2>
                <p class="subtitle"><?php echo $workshops->display('subtitle'); ?></p>
                <div class="text-and-calendar">
                    <div class="text"><?php echo $workshops->display('excerpt'); ?></div>
                    <div class="calendar non-mobile">
                        <div class="calendar-upper">
                            <div class="day"><?php echo $dates['start_day']; ?></div>
                            <div class="month"><?php echo $dates['start_month']; ?></div>
                        </div>
                        <div class="calendar-lower">
                            <div class="day"><?php echo $dates['end_day']; ?></div>
                            <div class="month"><?php echo $dates['end_month']; ?></div>
                        </div>
                    </div>
                </div>
                <div class="coaches-and-calendar">
                    <div class="calendar mobile">
                        <div class="calendar-upper">
                            <div class="day"><?php echo $dates['start_day']; ?></div>
                            <div class="month"><?php echo $dates['start_month']; ?></div>
                        </div>
                        <div class="calendar-lower">
                            <div class="day"><?php echo $dates['end_day']; ?></div>
                            <div class="month"><?php echo $dates['end_month']; ?></div>
                        </div>
                    </div>
                    <div class="coaches">
                        <?php $coaches            = $workshops->field('coaches'); ?>
                        <?php $i = 0; ?>
                        <?php foreach ($coaches as $coach_a): ?>
                        <?php 
                            $coach              = pods('coach', $coach_a['id']);
                            $coach_badge        = $coach->field('badge');
                            $coach_badge_url    = pods_image_url($coach_badge, null);
                            $coach_first_name   = $coach->field('first_name');
                            $i++;
                        ?>
                        <div class="coach coach-<?php echo $i; ?>">
                            <div class="coach-image">
                                <img src="<?php echo $coach_badge_url; ?>" />
                            </div>
                            <div class="coach-name"><?php echo $coach_first_name; ?></div>
                        </div>
                        <?php endforeach; ?>
                    </div>

                </div>
                <a class="about-info-button" href="<?php echo site_url('workshop/'.$workshops->field('permalink')); ?>">More information</a>
            </div>
        </div>
        <?php endwhile; ?>
        <div class="pods_pagination">
                <div class="advanced_pag">
                <?php 
                echo $workshops->pagination( array( 'type' => 'paginate', 'prev_text' => 'Prev', 'next_text' => 'Next', 'first_text' => 'First', 'last_text' => 'Last' ) ); 
                ?>
                </div>
        </div>
    </div>

</div>

<?php get_footer();